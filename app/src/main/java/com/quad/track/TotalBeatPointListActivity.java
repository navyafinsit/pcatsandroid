package com.quad.track;

/**
 * Created by DINU on 7/16/2018.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.quad.track.api.RequestEndPoints;
import com.quad.track.api.RetrofitClient;
import com.quad.track.model.BeatPointPOJO;
import com.quad.track.model.CommonPojo;
import com.quad.track.utils.AppConstants;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TotalBeatPointListActivity extends Activity {

    //ArrayAdapter arrayAdapter;
    ArrayList<String> dealersList=new ArrayList<String>();
    TotalBeatPointListAdapter beatPointAdapter;
    protected ProgressDialog progressDialog;

    ArrayList<BeatPointPOJO> allBeapointList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.totalbeatpointlist);

        //arrayAdapter=new ArrayAdapter<String>(this,R.layout.rowlayout,R.id.label, dealersList);

/*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.rowlayout, R.id.label, values);*/

       // getBeatpointsFromServer();

       // DBHelper dbHelper = new DBHelper(this);
        // SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

       // final  ArrayList<DealerBean> beatPoints=dbHelper.getAlarm();
        allBeapointList=(ArrayList<BeatPointPOJO>) getIntent().getExtras().get("beatPointsList");
        beatPointAdapter=new TotalBeatPointListAdapter(this,allBeapointList);


     /*   for(DealerBean db:beatPoints)
        {
            arrayAdapter.add(db.getDealerName()+ " "+ db.getDefinedInTime() + " " +db.getDefinedOutTime());
        }*/



        ListView lv =(ListView)findViewById(R.id.totallist);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.totallistheader, lv,false);
        lv.addHeaderView(header);
        lv.setAdapter(beatPointAdapter);
        lv.setDivider(new ColorDrawable(0x99FFFFFF));
        lv.setDividerHeight(2);

       // dbHelper.close();


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(BeatPointListActivity.this,"Hello"+position,Toast.LENGTH_LONG).show();
              /*  DealerBean db= beatPointAdapter.itemList.get(position-1);
                if("QR".equalsIgnoreCase(db.getBeatPointType()))
                {
                    Intent intent = new Intent(TotalBeatPointListActivity.this, NewQRScanActivity.class);
                    intent.putExtra("dealerId",db.getDealerId());
                    startActivity(intent);
                }else
                {
                    Intent intent = new Intent(TotalBeatPointListActivity.this, StartPage.class);
                    intent.putExtra("dealerId",db.getDealerId());
                    startActivity(intent);
                }
*/

            }
        });




        ImageButton addBeatPointButton=(ImageButton)findViewById(R.id.addBeatPointBtn);
        addBeatPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpMenu(v);
            }
        });

        ImageButton showMapPointBtn=(ImageButton)findViewById(R.id.showMapPointBtn);
       /* showMapPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showPopUpMenu(v);

                Intent i = new Intent(TotalBeatPointListActivity.this, LocationActivity.class);
                i.putExtra("beatPoints",beatPoints);
                startActivity(i);
            }
        });*/



        progressDialog = new ProgressDialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

    }




    private void getBeatpointsFromServer()
    {
        SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
        String deviceid = pref.getString("deviceid", null);
        Map<String,String> map=new HashMap<String, String>();
        map.put("deviceid",deviceid);

        Call<CommonPojo> call = RetrofitClient.getRestClient().getBeatPointList(RequestEndPoints.allbeats,map);


       try{
           Response<CommonPojo> response=call.execute();
           try {
               progressDialog.dismiss();

               if (response.isSuccessful()) {
                   if (response.body().getMessage() != null && !response.body().getMessage().equalsIgnoreCase("Successful"))
                       showToast(response.body().getMessage());
                   if(response.body().getStatus())
                   {
                       // webResponseCallback.onResponse(response.body());
                       allBeapointList=new ArrayList(response.body().getBeatPointPOJOList());
                       beatPointAdapter=new TotalBeatPointListAdapter(TotalBeatPointListActivity.this,allBeapointList);
                   }
                   else
                   {
                       allBeapointList=new ArrayList<BeatPointPOJO>();
                   }

               } else {
                   JSONObject jObjError = new JSONObject(response.errorBody().string());
                   showToast(jObjError.getString("message"));

                   allBeapointList=new ArrayList<BeatPointPOJO>();
               }
           } catch (Exception e) {
               e.printStackTrace();
           }



       }catch (Exception e)
       {
           e.printStackTrace();
       }


    /*    call.enqueue(new Callback<CommonPojo>() {

            @Override
            public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                try {
                    progressDialog.dismiss();

                    if (response.isSuccessful()) {
                        if (response.body().getMessage() != null && !response.body().getMessage().equalsIgnoreCase("Successful"))
                            showToast(response.body().getMessage());
                        if(response.body().getStatus())
                        {
                            // webResponseCallback.onResponse(response.body());
                            allBeapointList=new ArrayList(response.body().getBeatPointPOJOList());
                            beatPointAdapter=new TotalBeatPointListAdapter(TotalBeatPointListActivity.this,allBeapointList);
                        }
                        else
                        {
                            allBeapointList=new ArrayList<BeatPointPOJO>();
                        }

                    } else {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        showToast(jObjError.getString("message"));

                        allBeapointList=new ArrayList<BeatPointPOJO>();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<CommonPojo> call, Throwable t) {
                try {
                    progressDialog.dismiss();
                   // webResponseCallback.failure();
                    t.printStackTrace();
                    if (t instanceof SocketTimeoutException || t instanceof ConnectException)
                        showToast("Error Connecting to server");
                } catch (Exception e) {

                }
            }
        });*/
    }

    protected void showToast(String str) {
        Toast.makeText(TotalBeatPointListActivity.this, str, Toast.LENGTH_SHORT).show();
    }

    private void showPopUpMenu(final View imageView) {
        PopupMenu mMenu = new PopupMenu(TotalBeatPointListActivity.this, imageView);
        mMenu.getMenuInflater().inflate(R.menu.popup_image_selection, mMenu.getMenu());
        mMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = null;
                switch (item.getItemId()) {
                    case R.id.OptionBeatPoint:
                        i = new Intent(TotalBeatPointListActivity.this, AddBeatPoint.class);
                        i.putExtra("option","BP");
                        startActivity(i);

                        break;

                    case R.id.OptionBadCharacter:
                        i = new Intent(TotalBeatPointListActivity.this, AddBeatPoint.class);
                        i.putExtra("option","BC");
                        startActivity(i);
                        break;

                    case R.id.OptionQrCode:
                        i = new Intent(TotalBeatPointListActivity.this, NewQRScanActivity.class);
                        i.putExtra("option","QR");
                        startActivity(i);
                        break;
                }
                return false;
            }
        });
        mMenu.show();
    }

}
