package com.quad.track;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Date;

public class DataUploader {
	public static String uploadImageUri;
	public static int dd = 0, mm = 0, yyyy = 0, hr = 0, min = 0;
	public static float geocode[] = new float[2];
	public static String locAddr = "";
	public static String title = "";
	public static String detail = "";
	public static String latitude;
	public static String longitude;
	public static Date date;
	public static String ampm;
	public static String imagePath;
	public static String uploadStatus;
	public static String deviceId;
	public static String fingerPrintImageUrl;
	public static String fingerImagePath="Template"+ System.currentTimeMillis();

	public static String address="";
	public static String bptype="BP";
	public static String qrcode="";
	public static String routeCoordinateId="0";


/*	String time="";
	String status="";
	DBHelper dbh;
	public void insertIntoLocalDatabase(boolean insertStatus){
		time=DataUploader.hr+":"+DataUploader.mm+ampm;
		String year=DataUploader.yyyy+"".toString().trim();
		String month=DataUploader.mm+"".toString().trim();
		String day=DataUploader.dd+"".toString().trim();
		if(insertStatus==true){
			dbh.insertCrime(year,month,day,time, latitude, longitude, URLEncoder.encode(title),URLEncoder.encode(detail),imagePath,"S");
		}else{
			dbh.insertCrime(year,month,day,time, latitude, longitude, URLEncoder.encode(title),URLEncoder.encode(detail),imagePath,"F");
		}
	}*/
	
	public static boolean upload() {
		try{
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		String pathToOurFile = uploadImageUri;
		DataUploader du = new DataUploader();
		//imagePath="IMAGE"+System.currentTimeMillis()+".jpg";
		GetAvailability task=du.new GetAvailability();
		String urlServer="";
		if (uploadStatus!=null && uploadStatus.equalsIgnoreCase("F")) {
			urlServer = Identity.rootUrl + "StoreData?year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+
					"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title) +
					"&detail="+ URLEncoder.encode(detail)+"&userid=1"+"&fileName="+ DataUploader.imagePath;
		}else{
			imagePath= System.currentTimeMillis()+"_bmic.jpg".toString().trim();
			urlServer = Identity.rootUrl + "StoreData?year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+
					"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title) +
					"&detail="+ URLEncoder.encode(detail)+"&userid=1"+"&fileName="+imagePath;
		}

		String saveQurey=urlServer.replace(" ","%20");
		task.execute(urlServer);
		System.out.println(saveQurey);
		System.out.println(uploadImageUri);
		uploadPictureToServer(uploadImageUri);
		System.out.println("**************** IMAGE UPLOAD COMPLETED********");
/*		String insertImageQuery=Identity.rootUrl+"SaveImage?imageId=1"+"&photo="+uploadImageUri;
		System.out.println("Save Image query:"+insertImageQuery);
		task.execute(insertImageQuery);
*/		return true;
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
		return false;

}
	public static boolean uploadToServer() {
		try{


		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		String pathToOurFile = uploadImageUri;
		DataUploader du = new DataUploader();


		//imagePath="IMAGE"+System.currentTimeMillis()+".jpg";
		GetAvailability task=du.new GetAvailability();
/*		String urlServer = Identity.rootUrl + "StoreData?year="+DataUploader.yyyy+"&month="+DataUploader.mm+"&day="+DataUploader.dd+"&hour="+DataUploader.hr+"&min="+DataUploader.min+"&ampm="+ampm+
				"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title) +
				"&detail="+URLEncoder.encode(detail)+"&userid=1"+"&fileName="+imagePath;*/
		String urlServer = "";
		if (uploadStatus!=null && uploadStatus.equalsIgnoreCase("F")) {
			urlServer = Identity.rootUrl + "SaveCrimeCoordinates?year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+
					"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title) +
					"&detail="+ URLEncoder.encode(detail)+"&deviceId="+ DataUploader.deviceId+"&fileName="+ DataUploader.imagePath+"&fingerPrintImageUrl="+ DataUploader.fingerImagePath;
		}else{
			imagePath= System.currentTimeMillis()+"_bmic.jpg".toString().trim();
			urlServer = Identity.rootUrl + "SaveCrimeCoordinates?year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+
					"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title) +
					"&detail="+ URLEncoder.encode(detail)+"&deviceId="+ DataUploader.deviceId+"&fileName="+imagePath+"&fingerPrintImageUrl="+ DataUploader.fingerImagePath;
		}

		String saveQurey=urlServer.replace(" ","%20");
		task.execute(urlServer);
		System.out.println(saveQurey);
		System.out.println(uploadImageUri);
		uploadPictureToServer(uploadImageUri); //uploading image 
		System.out.println("**************** IMAGE UPLOAD COMPLETED********");
/*		String insertImageQuery=Identity.rootUrl+"SaveImage?imageId=1"+"&photo="+uploadImageUri;
		System.out.println("Save Image query:"+insertImageQuery);
		task.execute(insertImageQuery);
*/		return true;
		}
		catch(Exception e){
			
			e.printStackTrace();
		}
		return false;

}
		
		
	 
	public static boolean uploadPictureToServer(String i_file) throws ClientProtocolException, IOException {
	    // TODO Auto-generated method stub   
		try{
			System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER");
			imagePath="PID"+ System.currentTimeMillis()+".jpg".toString().trim();
			HttpClient httpclient = new DefaultHttpClient();
		    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		    String postURL= Identity.rootUrl+"BeatPhotoUploadServlet?imageUrl="+imagePath+"&fingerImageUrl="+fingerImagePath+"&year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title)+"&detail="+ URLEncoder.encode(detail)+"&deviceId="+ DataUploader.deviceId+"&fileName="+ DataUploader.imagePath+"&fingerPrintImageUrl="+ DataUploader.fingerImagePath+"&rcid="+ URLEncoder.encode(routeCoordinateId);
		    System.out.println(postURL);
		    HttpPost httppost = new HttpPost(postURL);
		    File file = new File(i_file);
		   /* MultipartEntity mpEntity = new MultipartEntity();
		    ContentBody cbFile = new FileBody(file, "image/jpeg");
		    mpEntity.addPart("userfile", cbFile);
		    httppost.setEntity(mpEntity);
		    MultipartEntity mpEntityFinger = new MultipartEntity();
		    ContentBody cbFileFinger = new FileBody(new File("/storage/sdcard0/template.dat"), "image/jpeg");
		    mpEntityFinger.addPart("fingerUserfile", cbFileFinger);
		    httppost.setEntity(mpEntityFinger);*/
		    MultipartEntity mpEntity = new MultipartEntity();
		    ContentBody cbFile = new FileBody(file, "image/jpeg");
		    mpEntity.addPart("userfile", cbFile);
		    //ContentBody cbFileFinger = new FileBody(new File("/storage/sdcard0/template.dat"), "image/jpeg");
			ContentBody cbFileFinger = new FileBody(new File(i_file), "image/jpeg");
			mpEntity.addPart("fingerUserfile", cbFileFinger);
		    httppost.setEntity(mpEntity);
		    System.out.println("executing request " + httppost.getRequestLine());
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(response.getStatusLine().getStatusCode());
		    if (resEntity != null) {
		      System.out.println(EntityUtils.toString(resEntity));

		    }
		    if (resEntity != null) {
		      resEntity.consumeContent();
		    }
		    httpclient.getConnectionManager().shutdown();
			if(response.getStatusLine().getStatusCode()==200)
			{
				return true;
			}else
			{
				return false;
			}

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}


	public static boolean uploadBeatPointToServer(String i_file) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		try{
			System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER");
			imagePath="PID"+ System.currentTimeMillis()+".jpg".toString().trim();
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			//String postURL=Identity.rootUrl+"addbp?imageUrl="+imagePath+"&fingerImageUrl="+fingerImagePath+"&year="+DataUploader.yyyy+"&month="+DataUploader.mm+"&day="+DataUploader.dd+"&hour="+DataUploader.hr+"&min="+DataUploader.min+"&ampm="+ampm+"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title)+"&detail="+ URLEncoder.encode(detail)+"&deviceId="+DataUploader.deviceId+"&fileName="+DataUploader.imagePath+"&fingerPrintImageUrl="+DataUploader.fingerImagePath;
			String postURL= Identity.rootUrl+"addbp?imageUrl="+imagePath+"&fingerImageUrl="+fingerImagePath+"&year="+ DataUploader.yyyy+"&month="+ DataUploader.mm+"&day="+ DataUploader.dd+"&hour="+ DataUploader.hr+"&min="+ DataUploader.min+"&ampm="+ampm+"&latitude="+latitude+"&longitude="+longitude+"&title=" + URLEncoder.encode(title)+"&bpname="+ URLEncoder.encode(detail)+"&deviceId="+ DataUploader.deviceId+"&fileName="+ DataUploader.imagePath+"&fingerPrintImageUrl="+ DataUploader.fingerImagePath+"&bptype="+URLEncoder.encode(bptype)+"&address="+ URLEncoder.encode(address)+"&qrcode="+ URLEncoder.encode(qrcode);

			System.out.println(postURL);
			HttpPost httppost = new HttpPost(postURL);
			File file = new File(i_file);
		   /* MultipartEntity mpEntity = new MultipartEntity();
		    ContentBody cbFile = new FileBody(file, "image/jpeg");
		    mpEntity.addPart("userfile", cbFile);
		    httppost.setEntity(mpEntity);
		    MultipartEntity mpEntityFinger = new MultipartEntity();
		    ContentBody cbFileFinger = new FileBody(new File("/storage/sdcard0/template.dat"), "image/jpeg");
		    mpEntityFinger.addPart("fingerUserfile", cbFileFinger);
		    httppost.setEntity(mpEntityFinger);*/
			MultipartEntity mpEntity = new MultipartEntity();
			ContentBody cbFile = new FileBody(file, "image/jpeg");
			mpEntity.addPart("userfile", cbFile);
			//ContentBody cbFileFinger = new FileBody(new File("/storage/sdcard0/template.dat"), "image/jpeg");
			ContentBody cbFileFinger = new FileBody(new File(i_file), "image/jpeg");
			mpEntity.addPart("fingerUserfile", cbFileFinger);
			httppost.setEntity(mpEntity);
			System.out.println("executing request " + httppost.getRequestLine());
			HttpResponse response = httpclient.execute(httppost);


		/*	HttpEntity resEntity = response.getEntity();
			System.out.println(response.getStatusLine().getStatusCode());
			if (resEntity != null) {
				System.out.println(EntityUtils.toString(resEntity));

			}
			if (resEntity != null) {
				resEntity.consumeContent();
			}
			httpclient.getConnectionManager().shutdown();
			if(response.getStatusLine().getStatusCode()==200)
			{
				return true;
			}else
			{
				return false;
			}

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}*/



			boolean failure=false;

			HttpEntity resEntity = response.getEntity();
			System.out.println(response.getStatusLine());
			if (resEntity != null) {
				if("FAILURE".equalsIgnoreCase(EntityUtils.toString(resEntity)))
				{
					failure=true;
				}

			}
			if (resEntity != null) {
				resEntity.consumeContent();



			}
			httpclient.getConnectionManager().shutdown();
			if(failure)
			{
				return false;
			}return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	//INSERT THE DATA INTO DATABASE 
	final class  GetAvailability extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
        @Override
        protected String doInBackground(String... urls) {
            String output = null;
            for (String url : urls) {
                output = getOutputFromUrl(url);
            }
            return output;
        }
        private String getOutputFromUrl(String url) {
            String output = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                output = EntityUtils.toString(httpEntity);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
			return output; 
        }}
}