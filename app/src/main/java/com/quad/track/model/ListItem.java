package com.quad.track.model;

public class ListItem{
	private double latitude;
	private String beatPointType;
	private String qrCode;
	private String id;
	private int stopOrder;
	private double longitude;
	private String nAME;

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setBeatPointType(String beatPointType){
		this.beatPointType = beatPointType;
	}

	public String getBeatPointType(){
		return beatPointType;
	}

	public void setQrCode(String qrCode){
		this.qrCode = qrCode;
	}

	public String getQrCode(){
		return qrCode;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStopOrder(int stopOrder){
		this.stopOrder = stopOrder;
	}

	public int getStopOrder(){
		return stopOrder;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public void setNAME(String nAME){
		this.nAME = nAME;
	}

	public String getNAME(){
		return nAME;
	}

	@Override
 	public String toString(){
		return 
			"ListItem{" + 
			"latitude = '" + latitude + '\'' + 
			",beat_point_type = '" + beatPointType + '\'' + 
			",qr_code = '" + qrCode + '\'' + 
			",id = '" + id + '\'' + 
			",stop_order = '" + stopOrder + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",nAME = '" + nAME + '\'' + 
			"}";
		}
}
