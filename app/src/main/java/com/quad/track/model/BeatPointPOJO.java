package com.quad.track.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DINU on 7/17/2018.
 */

public class BeatPointPOJO implements java.io.Serializable{


    private double latitude;
    @SerializedName("beat_point_type")
    @Expose
    private String beatPointType;

    @SerializedName("qr_code")
    @Expose
    private Object qrCode;

    private String id;

    @SerializedName("stop_order")
    @Expose
    private int stopOrder;
    private double longitude;

    @SerializedName("NAME")
    @Expose
    private String nAME;

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    public double getLatitude(){
        return latitude;
    }


    public void setBeatPointType(String beatPointType){
        this.beatPointType = beatPointType;
    }

    public String getBeatPointType(){
        return beatPointType;
    }

    public void setQrCode(Object qrCode){
        this.qrCode = qrCode;
    }

    public Object getQrCode(){
        return qrCode;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setStopOrder(int stopOrder){
        this.stopOrder = stopOrder;
    }

    public int getStopOrder(){
        return stopOrder;
    }

    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public void setNAME(String nAME){
        this.nAME = nAME;
    }

    public String getNAME(){
        return nAME;
    }

    @Override
    public String toString(){
        return
                "Response{" +
                        "latitude = '" + latitude + '\'' +
                        ",beat_point_type = '" + beatPointType + '\'' +
                        ",qr_code = '" + qrCode + '\'' +
                        ",id = '" + id + '\'' +
                        ",stop_order = '" + stopOrder + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        ",nAME = '" + nAME + '\'' +
                        "}";
    }
}
