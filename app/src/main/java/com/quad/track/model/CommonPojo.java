package com.quad.track.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Armaan on 02-11-2017.
 */

public class CommonPojo {

    private boolean success;
    private Integer statusCode;
    private String message;
    private String error;
    private String responseType;


    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("allbeatpointslist")
    @Expose
    private List<BeatPointPOJO> beatPointPOJOList =null;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<BeatPointPOJO> getBeatPointPOJOList() {
        return beatPointPOJOList;
    }

    public void setBeatPointPOJOList(List<BeatPointPOJO> beatPointPOJOList) {
        this.beatPointPOJOList = beatPointPOJOList;
    }
}
