package com.quad.track;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.quad.track.model.BeatPointPOJO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by My Documents on 3/14/2017.
 */


public class TotalBeatPointListAdapter extends BaseAdapter{

    ArrayList<BeatPointPOJO> itemList=new ArrayList<BeatPointPOJO>();

    public Activity context;
    public LayoutInflater inflater;

    public TotalBeatPointListAdapter(Activity context, ArrayList<BeatPointPOJO> itemList) {
        super();

        this.context = context;

        this.itemList = itemList;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder
    {
        ImageView imgViewLogo;
        TextView txtViewTitle;
        TextView txtViewTimings;
        ImageView imgPictureIcom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder;
        if(convertView==null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.rowlayout, null);

            holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.icon);
            holder.txtViewTitle = (TextView) convertView.findViewById(R.id.label);
            holder.txtViewTimings = (TextView) convertView.findViewById(R.id.timings);
            holder.imgPictureIcom=(ImageView) convertView.findViewById(R.id.takepictureforattendance);



            convertView.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)convertView.getTag();
        }

        BeatPointPOJO bean = (BeatPointPOJO) itemList.get(position);

       if("BP".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.beatpoints);
       }else if("BC".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.bc);
       }else if("QR".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.qrcode);
           holder.imgPictureIcom.setImageResource(R.drawable.qr);
       }
        try{


            SimpleDateFormat sf=new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat newSf=new SimpleDateFormat("hh:mm a");
            holder.txtViewTitle.setTextColor(Color.parseColor("#ffffff"));

           // holder.txtViewTitle.setBackgroundColor(Color.parseColor("#3366FF"));
            convertView.setBackgroundColor(Color.parseColor("#3366FF"));

            holder.txtViewTitle.setTextColor(Color.parseColor("#ffffff"));
            holder.txtViewTimings.setTextColor(Color.parseColor("#ffffff"));


            holder.txtViewTitle.setText(bean.getNAME());
            //holder.txtViewTimings.setText(newSf.format(inTime) + "     " +newSf.format(outTime));



        }catch(Exception e)
        {
            e.printStackTrace();
        }


        return convertView;
    }

}