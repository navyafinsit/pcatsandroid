package com.quad.track;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class GenerateQRActivity extends Activity {

    public final static int WHITE = 0xFFFFFFFF;
    public final static int BLACK = 0x0000000;
    public final static int WIDTH = 300;
    public final static int HEIGHT = 300;
    public final static String STR = "DINESH";
    Bitmap finalbitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generateqr);
        ImageView imageView = (ImageView) findViewById(R.id.myImage);
        try {
           final Bitmap bitmap = encodeAsBitmap(STR);
            imageView.setImageBitmap(bitmap);
            finalbitmap=bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void printQR()
    {
       // finalbitmap

      /*  new Thread(new Runnable() {
            public void run() {
                try {

                    // Instantiate connection for given Bluetooth® MAC Address.
                    ZebraPrinterConnection thePrinterConn = new BluetoothPrinterConnection("XX:XX:XX:XX:XX:XX");

                    // Initialize
                    Looper.prepare();

                    // Open the connection - physical connection is established here.
                    thePrinterConn.open();


                    // Print QR code
                    ZebraPrinter printer = ZebraPrinterFactory.getInstance(thePrinterConn);
                    printer.getGraphicsUtil().printImage(bitmap, 75, 0, 250, 250, false);


                    //Make sure the data got to the printer before closing the connection
                    Thread.sleep(500);

                    // Close the connection to release resources.
                    thePrinterConn.close();

                    Looper.myLooper().quit();

                } catch (Exception e) {

                    // Handle communications error here
                    e.printStackTrace();

                }
            }
        }).start();*/

    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}