package com.quad.track;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Troubleshoot extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.troubleshoot);
		ImageView internet=(ImageView)findViewById(R.id.internetts);
		ImageView gps=(ImageView)findViewById(R.id.gpsts);
		ImageView deviceId=(ImageView)findViewById(R.id.deviceidts);
		TextView deviceIdText=(TextView)findViewById(R.id.deviceidtext);
		ImageView beatpoints=(ImageView)findViewById(R.id.beatpointsts);

		Button activateButton=(Button) findViewById(R.id.activateDeviceButton);

		activateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Troubleshoot.this, TestgpsActivity.class);
				startActivity(intent);
				finish();
			}
		});

		Button updateApp=(Button) findViewById(R.id.updateapp);

		updateApp.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Troubleshoot.this, MainActivity.class);
				startActivity(intent);
				//finish();
			}
		});





		SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		String empid = pref.getString("deviceid", null);
		if (empid == null) {
			deviceId.setImageResource(R.drawable.no);
			deviceIdText.setVisibility(View.INVISIBLE);
			activateButton.setVisibility(View.VISIBLE);
		}else
		{
			deviceId.setImageResource(R.drawable.yes);
			deviceIdText.setText(empid);
		}

		ConnectionDetector connectionDetector = new ConnectionDetector(Troubleshoot.this);
		if(connectionDetector.isConnectingToInternet()) {
			internet.setImageResource(R.drawable.yes);

		}else
		{
			internet.setImageResource(R.drawable.no);
		}

		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(isGPSEnabled)
		{
			gps.setImageResource(R.drawable.yes);
		}else
		{
			gps.setImageResource(R.drawable.no);
		}

		DBHelper db=new DBHelper(getApplicationContext());

		// added by dinesh
		if(db.getAlarm()==null || db.getAlarm().size()==0) // which means there are no beat points.. so send upload as success
		{
			beatpoints.setImageResource(R.drawable.no);
		}else
		{
			beatpoints.setImageResource(R.drawable.yes);
		}
		db.close();



	}

}
