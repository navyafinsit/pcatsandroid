package com.quad.track;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//import android.support.v7.app.AppCompatActivity;


public class QRMainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_activity_main);
    }

    /* public void scanBarcode(View view) {
         new IntentIntegrator(this).initiateScan();
     }

     public void scanBarcodeInverted(View view){
         IntentIntegrator integrator = new IntentIntegrator(this);
         integrator.addExtra(Intents.Scan.INVERTED_SCAN, true);
         integrator.initiateScan();
     }

     public void scanBarcodeCustomLayout(View view) {
         IntentIntegrator integrator = new IntentIntegrator(this);
         integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
         integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
         integrator.setPrompt("Scan something");
         integrator.setOrientationLocked(false);
         integrator.setBeepEnabled(false);
         integrator.initiateScan();
     }

     public void scanBarcodeFrontCamera(View view) {
         IntentIntegrator integrator = new IntentIntegrator(this);
         integrator.setCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT);
         integrator.initiateScan();
     }

     public void scanContinuous(View view) {
         Intent intent = new Intent(this, ContinuousCaptureActivity.class);
         startActivity(intent);
     }

     public void scanToolbar(View view) {
         new IntentIntegrator(this).setCaptureActivity(ToolbarCaptureActivity.class).initiateScan();
     }

     public void scanCustomScanner(View view) {
         new IntentIntegrator(this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan();
     }
 */
    public void scanMarginScanner(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setCaptureActivity(SmallCaptureActivity.class);
        integrator.initiateScan();
    }

    public void punchQR(View view) {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // getting GPS status
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(!isGPSEnabled)
        {
            showSettingsAlert();
        }else {

            final Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if(location==null)
            {
                Toast.makeText(getApplicationContext(),"Sorry Your location is not available",Toast.LENGTH_LONG).show();
                return;
            }
            DBHelper db = new DBHelper(getApplicationContext());

            ArrayList<DealerBean> nearestQRcodes = db.getNearstQRBean(location.getLatitude(), location.getLongitude());
            db.close();

            if (nearestQRcodes != null && nearestQRcodes.size() > 0) {

                final DealerBean matchedQrBean = nearestQRcodes.get(0);
                new AlertDialog.Builder(this)
                    .setMessage(matchedQrBean.getDealerName())
                    .setTitle("Manual QR")
                    .setCancelable(true)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            finish();

                        }
                    })
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    {
                                        boolean isCorrectTime=calculateTime(matchedQrBean.getDefinedInTime(),matchedQrBean.getDefinedOutTime());
                                        if(!isCorrectTime)
                                        {
                                            Toast.makeText(getApplicationContext(),"Sorry You are Out of Time range!",Toast.LENGTH_LONG).show();
                                        }else
                                        {
                                            UploadQRCodeDetails task = new UploadQRCodeDetails();
                                            String empid = matchedQrBean.getEmpid();
                                            String dealerID = matchedQrBean.getDealerId();
                                            String qrTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

                                            String lat = location.getLatitude() + ""; // must replace this with locaition Manager..
                                            String lng = location.getLongitude() + "";
                                            task.execute(empid, dealerID, qrTime, lat, lng, matchedQrBean.getQrcode());

                                            finish();
                                        }

                                        } // end on onclick



                                    }



                            }).show();
            }else
            {
                Toast.makeText(getApplicationContext(),"Sorry QR code not in the Range",Toast.LENGTH_LONG).show();

            }

        }
    }



    public boolean calculateTime(String inTime,String outTime)
    {


        Calendar cal = Calendar.getInstance();

        Time currentTime = Time.valueOf(
                cal.get(Calendar.HOUR_OF_DAY) + ":" +
                        cal.get(Calendar.MINUTE) + ":" +
                        cal.get(Calendar.SECOND) );


        try{
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

            long ms1 = sdf.parse(inTime).getTime();
            Time t1 = new Time(ms1);
            long ms2 = sdf.parse(outTime).getTime();
            Time t2 = new Time(ms2);

            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(t1);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(t2);

            int x1=Calendar.PM;
            int y1=cal1.get(Calendar.AM_PM);
            int x2=Calendar.AM;
            int y2=cal2.get(Calendar.AM_PM);


            if(x1==y1 && x2==y2) // THIS IS FOR PM AM
            {
                //CALENDAR FOR 12 AM
                Calendar cal12AM = Calendar.getInstance();
                //cal12AM.add(Calendar.DATE,1);// ADDING 1 TO GET ZERO HOUR OF NEXT DAY....
                cal12AM.set(Calendar.HOUR_OF_DAY,23);
                cal12AM.set(Calendar.MINUTE,59);
                cal12AM.set(Calendar.SECOND,59);
                Time zeroTime1=Time.valueOf(	cal12AM.get(Calendar.HOUR_OF_DAY) + ":" +
                        cal12AM.get(Calendar.MINUTE) + ":" +
                        cal12AM.get(Calendar.SECOND) );

                Time zeroTime2=Time.valueOf("00:00:00");

                if(currentTime.after(t1) && currentTime.before(zeroTime1) ||  currentTime.after(zeroTime2) && currentTime.before(t2))
                {
                    return true;
                }else
                {
                    return false;
                }
            }



            if(currentTime.after(t1) && currentTime.before(t2)) // this if for AM AM , AM PM , PM PM
            {
                return true;
            }else
            {
                return false;
            }

        }catch(java.text.ParseException pe)
        {
            pe.printStackTrace();
        }



        return false;

    }
    /*public void scanWithTimeout(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setTimeout(8000);
        integrator.initiateScan();
    }

    public void tabs(View view) {
        Intent intent = new Intent(this, TabbedScanning.class);
        startActivity(intent);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.d("PrintQRMainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("PrintQRMainActivity", "Scanned");
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();

                String qrcode=result.getContents();
                alertboxToConfigure("Confirm", result.getContents(),qrcode);

            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void alertboxToConfigure(String title, String msg, final String qrcode) {
        if (msg != null) {
            new AlertDialog.Builder(this)
                    .setMessage(msg)
                    .setTitle(title)
                    .setCancelable(true)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            finish();

                        } })
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);




                                    // getting GPS status
                                    boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                    if(!isGPSEnabled)
                                    {
                                        showSettingsAlert();
                                    }else
                                    {

                                        Location location=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


                                        DBHelper db=new DBHelper(getApplicationContext());
                                        ArrayList<DealerBean> qrcodes=db.getQRBean(qrcode);
                                        db.close();

                                        if(qrcodes!=null && qrcodes.size()>0) {
                                            DealerBean matchedQrBean=qrcodes.get(0);

                                            UploadQRCodeDetails task=new UploadQRCodeDetails();


                                            String empid=matchedQrBean.getEmpid();
                                            String dealerID=matchedQrBean.getDealerId();
                                            String qrTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                                            String lat=location.getLatitude()+""; // must replace this with locaition Manager..
                                            String lng=location.getLongitude()+"";

                                            double distance=findDistance(location.getLatitude(),location.getLongitude(),Double.parseDouble(matchedQrBean.getLatitude()),Double.parseDouble(matchedQrBean.getLongitude()));

                                            if( distance*1000  < 30 ) // qr code is scanned in the correct location
                                            {
                                                boolean isCorrectTime=calculateTime(matchedQrBean.getDefinedInTime(),matchedQrBean.getDefinedOutTime());
                                                if(!isCorrectTime)
                                                {
                                                    Toast.makeText(getApplicationContext(),"Sorry You are Out of Time range!",Toast.LENGTH_LONG).show();
                                                }else{
                                                    task.execute(empid,dealerID,qrTime,lat,lng,qrcode);
                                                }



                                                finish();

                                            }else
                                            {
                                                Toast.makeText(getApplicationContext(),"Sorry QR code not in the Range",Toast.LENGTH_LONG).show();
                                            }



                                        }else
                                        {
                                            Toast.makeText(getApplicationContext(),"Sorry QR Code not listed in this beat abstract",Toast.LENGTH_LONG).show();
                                        }




                                    }


                                } // end of onclick
                            }).show();
        }
    }

    private double findDistance(double lat, double lon, double currentlat, double currentlon) {

        double dist = 0.0;
        double deltaLat = Math.toRadians(currentlat - lat);
        double deltaLon = Math.toRadians(currentlon - lon);
        lat = Math.toRadians(lat);
        currentlat = Math.toRadians(currentlat);
        lon = Math.toRadians(lon);
        currentlon = Math.toRadians(currentlon);
        double earthRadius = 6371;
        double a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
                Math.cos(lat) * Math.cos(currentlat) * Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        dist = earthRadius * c;
        return dist;

    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Go to settings menu?");
        alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        AlertDialog alertDialog1=alertDialog.create();
        alertDialog1.show();
    }
    private class UploadQRCodeDetails extends AsyncTask<String, Void, String> {
        String output = null;
        String data = "";
        private final HttpClient Client = new DefaultHttpClient();
        private String Content;
        private String Error = null;
        private boolean noRecords = false;
        private String empid;



        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {


            String responseString = null;
            DBHelper db=new DBHelper(getApplicationContext());
            String empid=urls[0];
            String dealerid=urls[1];
            String qrtime=urls[2];
            String lat=urls[3];
            String lng=urls[4];
            String qrcode=urls[5];

            try
            {

                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);



                String urlStr = IPInfo.HTTP_IPADDRESS+"/vehicleservice/qrattendance?empid="+empid+"&dealerid="+dealerid+"&qrcode="+qrcode+"&qrtime="+qrtime+"&lat="+lat+"&lng="+lng;

                urlStr = urlStr.toString().replace(" ", "%20");
                //URL url = new URL(urlStr);
                HttpPost httppost = new HttpPost(urlStr);

                HttpResponse response = httpclient.execute(httppost);
                httpclient.getConnectionManager().shutdown();
                //db.updateBeatVisitedStatus(matchedQrBean.getDealerId(),"Y");
                db.updateQRVisitedStatus(empid,dealerid,lat,lng,"Y",true);

            }catch(Exception e)
            {
                if(e instanceof java.io.IOException) // add the data to beatvisitdetails.
                {
                    db.updateQRVisitedStatus(empid,dealerid,lat,lng,"Y",false);
                }
                e.printStackTrace();

                return null;
            }finally {
                db.close();
            }
            return responseString;


        }


    }






}