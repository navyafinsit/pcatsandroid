package com.quad.track;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.hoin.btsdk.BluetoothService;
import com.hoin.btsdk.PrintPic;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class BlueToothFragment extends Fragment {
	Button btnSearch;
	Button btnSendDraw;
	Button btnSend;
	Button btnClose;
	EditText edtContext;
	EditText edtPrint;
	private static final int REQUEST_ENABLE_BT = 2;
	BluetoothService mService = null;
	BluetoothDevice con_dev = null;
	private View qrCodeBtnSend;
	private static final int REQUEST_CONNECT_DEVICE = 1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.bluetooth_fragment, null);
		btnSendDraw = (Button)view.findViewById(R.id.btn_test);
		btnSendDraw.setOnClickListener(new ClickEvent());
		btnSearch = (Button)view.findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new ClickEvent());
		btnSend = (Button)view.findViewById(R.id.btnSend);
		btnSend.setOnClickListener(new ClickEvent());
		qrCodeBtnSend = (Button)view.findViewById(R.id.qr_code_Send);
		qrCodeBtnSend.setOnClickListener(new ClickEvent());
		btnClose = (Button)view.findViewById(R.id.btnClose);
		btnClose.setOnClickListener(new ClickEvent());
		edtContext = (EditText)view.findViewById(R.id.txt_content);
		btnClose.setEnabled(false);
		//btnSend.setEnabled(false);
		qrCodeBtnSend.setEnabled(false);
		btnSendDraw.setEnabled(false);

		//added by dinesh
		ImageView imageView =  (ImageView)view.findViewById(R.id.myImage);
		try {
			final Bitmap bitmap = encodeAsBitmap(STR);
			imageView.setImageBitmap(bitmap);
			finalbitmap=bitmap;
		} catch (WriterException e) {
			e.printStackTrace();
		}




		return view;
	}

	public final static int WHITE = 0xFFFFFFFF;
	public final static int BLACK = 0xFF000000;
	public final static int WIDTH = 400;
	public final static int HEIGHT = 400;
	public final static String STR = "DINESH";
	Bitmap finalbitmap;
	File savedFile;




	Bitmap encodeAsBitmap(String str) throws WriterException {
		BitMatrix result;
		try {
			result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, null);
		} catch (IllegalArgumentException iae) {
			// Unsupported format
			return null;
		}

		int width = result.getWidth();
		int height = result.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
			}
		}

		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

		return bitmap;
	}



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mService = new BluetoothService(getActivity(), mHandler);

		if( mService.isAvailable() == false ){
			Toast.makeText(getActivity(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
		}	
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if( mService.isBTopen() == false){
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mService != null) 
			mService.stop();
		mService = null; 
	}
	
	class ClickEvent implements View.OnClickListener {
		public void onClick(View v) {
			String msg = "";
			switch (v.getId()) {
			case R.id.btnSend:
				try{
					//byte[] command = Utils.decodeBitmap(finalbitmap);

					File f = new File(BlueToothFragment.this.getContext() .getCacheDir(), "temp");
					f.createNewFile();

					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					finalbitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byte[] byteArray = stream.toByteArray();

					FileOutputStream fos = new FileOutputStream(f);
					fos.write(byteArray);
					fos.flush();
					fos.close();

				/*	byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, (byte)255, 3};
					//
					mService.write(SELECT_BIT_IMAGE_MODE);
					//cmd[2] &= byteArray;
					mService.write(byteArray);*/
					printImage(f);
					mService.write(new byte[]{27, 97, 1});
					mService.sendMessage("AHOBILAM MATT B LANE","GBR");
					break;

				}catch (Exception e)
				{
					e.printStackTrace();
				}



			/*	byte[] cmd = new byte[7];
				cmd[0] = 0x1B;
				cmd[1] = 0x5A;
				cmd[2] = 0x00;
				cmd[3] = 0x02;
				cmd[4] = 0x07;
				cmd[5] = 0x17;
				cmd[6] = 0x00;*/

			case R.id.btnSearch:
				Intent serverIntent = new Intent(getActivity(),DeviceListActivity.class);      //��������һ����Ļ
				startActivityForResult(serverIntent,REQUEST_CONNECT_DEVICE);
				break;

			}
		}
	}

	/**
	 * ����һ��Handlerʵ�������ڽ���BluetoothService�෵�ػ�������Ϣ
	 */
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case BluetoothService.MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BluetoothService.STATE_CONNECTED:   //������
					Toast.makeText(getActivity(), "Connect successful",
							Toast.LENGTH_SHORT).show();
					btnClose.setEnabled(true);
					btnSend.setEnabled(true);
					qrCodeBtnSend.setEnabled(true);
					btnSendDraw.setEnabled(true);
					break;
				case BluetoothService.STATE_CONNECTING:  //��������
					Log.d("��������","��������.....");
					break;
				case BluetoothService.STATE_LISTEN:     //�������ӵĵ���
				case BluetoothService.STATE_NONE:
					Log.d("��������","�ȴ�����.....");
					break;
				}
				break;
			case BluetoothService.MESSAGE_CONNECTION_LOST:    //�����ѶϿ�����
				Toast.makeText(getActivity(), "Device connection was lost",
						Toast.LENGTH_SHORT).show();
				btnClose.setEnabled(false);
				btnSend.setEnabled(false);
				qrCodeBtnSend.setEnabled(false);
				btnSendDraw.setEnabled(false);
				break;
			case BluetoothService.MESSAGE_UNABLE_CONNECT:     //�޷������豸
				Toast.makeText(getActivity(), "Unable to connect device",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}

	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_ENABLE_BT:      //���������
			if (resultCode == Activity.RESULT_OK) {   //�����Ѿ���
				Toast.makeText(getActivity(), "Bluetooth open successful", Toast.LENGTH_LONG).show();
			}
			break;
		case  REQUEST_CONNECT_DEVICE:     //��������ĳһ�����豸
			if (resultCode == Activity.RESULT_OK) {   //�ѵ�������б��е�ĳ���豸��
				String address = data.getExtras()
						.getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //��ȡ�б������豸��mac��ַ
				con_dev = mService.getDevByMac(address);   

				mService.connect(con_dev);
			}
			break;
		}
	} 

	//��ӡͼ��
	@SuppressLint("SdCardPath")
	private void printImage(File f) {


		byte[] sendData = null;
		PrintPic pg = new PrintPic();
		pg.initCanvas(400);
		pg.initPaint();
		pg.drawImage(0, 0, f.getAbsolutePath());
		//
		sendData = pg.printDraw();
		mService.write(sendData);   //��ӡbyte������
		Log.d("��������",""+sendData.length);
	}



}
