package com.quad.track;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DBHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "cravaka.db";
	private static final int DB_VERSION = 1;
	private static final String DEALER_DETAILS = "dealer_details";
	private static final String ID_FIELD = "_id";
	private static final String DEALER_ID = "dealer_id";
	private static final String EMP_ID = "emp_id";
	private static final String DEALER_NAME = "dealer_name";
	private static final String LATITUDE_FIELD = "dealer_lat";
	private static final String LONGITUDE_FIELD = "dealer_lon";
	private static final String STATE_FIELD = "state";
	private static final String DISABLED = "disabled";
	private static final String IN_TIME = "intime";
	private static final String OUT_TIME = "outtime";

	private static final String BEAT_POINT_TYPE="beat_point_type";
	private static final String VISITED="visited";
	private static final String QR_CODE="qr_code";




	public static final String DEALER_VISIT_DETAILS_TABLE_NAME = "dealer_visit_details";
	private static final String ID = "_id";
	public static final String DATE = "date";
	public static final String DEALERS_ID = "dealers_id";
	public static final String EMPID = "empid";
	public static final String ENTRY_TIME = "entry_time";
	public static final String EXIT_TIME = "exit_time";
	public static final String TIME_SPENT = "time_spent";
	private double timespent;
	private SimpleDateFormat datePattern,timePattern;

	/*private static final String SQL="create table dealer_details " +
			"(_id integer primary key, dealer_id text, emp_id text, dealer_name text, dealer_lat text, dealer_lon text, state INTEGER)";*/


	private static final String SQL="create table dealer_details " +
			"(_id integer primary key, dealer_id text, emp_id text, dealer_name text, dealer_lat text, dealer_lon text, state INTEGER,disabled text,intime text,outtime text,beat_point_type text,visited text,qr_code text)";


	String SQL2 = "create table dealer_visit_details "+" (_id integer primary key, date text, dealers_id text,"
			+ "empid text, entry_time text, exit_time text, time_spent text)";

	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL);
		db.execSQL(SQL2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
/*	public long insertDealer(String empid, String dealer_id, String dealer_name, String dealer_lat, String dealer_lon) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LATITUDE_FIELD, dealer_lat);
		values.put(LONGITUDE_FIELD, dealer_lon);
		values.put(DEALER_NAME, dealer_name);
		values.put(DEALER_ID, dealer_id);
		values.put(EMP_ID, empid);
		values.put(STATE_FIELD, 0);
		long insert = db.insert(DEALER_DETAILS, null, values);
		db.close();
		return insert;
	}*/

	//overloaded method add by dinesh for adding disabled, intime and out time.
	public long insertDealer(String empid, String dealer_id, String dealer_name, String dealer_lat, String dealer_lon, boolean disabled, String inTime,String outTime,String beatPointType,String qrCode) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LATITUDE_FIELD, dealer_lat);
		values.put(LONGITUDE_FIELD, dealer_lon);
		values.put(DEALER_NAME, dealer_name);
		values.put(DEALER_ID, dealer_id);
		values.put(EMP_ID, empid);
		values.put(STATE_FIELD, 0);
		//values
		values.put(DISABLED,disabled);
		values.put(IN_TIME,inTime);
		values.put(OUT_TIME,outTime);

		//add beat point type.
		values.put(BEAT_POINT_TYPE,beatPointType);
		values.put(QR_CODE,qrCode);

		long insert = db.insert(DEALER_DETAILS, null, values);
		db.close();
		return insert;
	}

	public long insertDealerVisitDetails(String date, String dealers_id, String empid, String entry_time, String exit_time, String time_spent)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("date", date);
		contentValues.put("dealers_id", dealers_id);
		contentValues.put("empid", empid);
		contentValues.put("entry_time", entry_time);
		contentValues.put("exit_time", exit_time);	
		contentValues.put("time_spent", time_spent);
		long insert = db.insert(DEALER_VISIT_DETAILS_TABLE_NAME, null, contentValues);
		db.close();
		return insert;
	}
	public boolean updateDealerVisitDetails (Integer id, String date, String dealers_id, String empid, String entry_time, String exit_time, String time_spent)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("date", date);
		contentValues.put("dealers_id", dealers_id);
		contentValues.put("empid", empid);
		contentValues.put("entry_time", entry_time);
		contentValues.put("exit_time", exit_time);	
		contentValues.put("time_spent", time_spent);
		db.update("dealer_visit_details", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
		db.close();
		return true;
	}
	public boolean updateExitTime (Integer id, String exit_time, String time_spent)
	{
		System.out.println(" integer id:"+ id+ " Exit time: "+ exit_time);

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("exit_time", exit_time);
		db.update("dealer_visit_details", contentValues, "_id = ? ", new String[] { Integer.toString(id) } );
		//UPDATE MAIN TABLE SO THAT VISIT STATUS IS SHOWN TO BEAT PERSON.

		db.close();
		return true;
	}

	public boolean updateBeatVisitedStatus(String id,String status)
	{
		System.out.println(" updated id:"+ id+ " visited status: "+ status);

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("visited", status);
		db.update("dealer_details", contentValues, "dealer_id = ? ", new String[] { id } );
		//UPDATE MAIN TABLE SO THAT VISIT STATUS IS SHOWN TO BEAT PERSON.

		db.close();
		return true;
	}

	public boolean updateQRVisitedStatus(String empid,String id,String lat, String lng, String status,boolean uploadSuccess)
	{
		System.out.println(" updated id:"+ id+ " visited status: "+ status);
		SimpleDateFormat sf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sfDate=new SimpleDateFormat("yyyy-MM-dd");
		String currentTime=sf.format(new Date());
		String date=sfDate.format(new Date());

		SQLiteDatabase db = this.getWritableDatabase();

		//update dealer table to see the green color
		ContentValues content = new ContentValues();
		content.put("visited", status);
		db.update("dealer_details", content, "dealer_id = ? ", new String[] { id } );

		ContentValues contentValues = new ContentValues();
		contentValues.put("date", date);
		contentValues.put("dealers_id", id);
		contentValues.put("empid", empid);
		contentValues.put("entry_time", currentTime);
		contentValues.put("exit_time", currentTime);
		if(uploadSuccess)
		{
			contentValues.put("time_spent", 1);
		}else
		{
			contentValues.put("time_spent", 2);
		}
		long insert = db.insert(DEALER_VISIT_DETAILS_TABLE_NAME, null, contentValues);



		db.close();
		return true;
	}


	public boolean updateQRVisitedStatus (Integer id, String date, String dealers_id, String empid, String entry_time, String exit_time, String time_spent)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("date", date);
		contentValues.put("dealers_id", dealers_id);
		contentValues.put("empid", empid);
		contentValues.put("entry_time", entry_time);
		contentValues.put("exit_time", exit_time);
		contentValues.put("time_spent", time_spent);
		db.update("dealer_visit_details", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
		db.close();
		return true;
	}


/*	public ArrayList<DealerBean> getAlarm() {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(DEALER_DETAILS, null, null, null, null, null, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				String empid = cursor.getString(cursor.getColumnIndex(EMP_ID));
				String title = cursor.getString(cursor.getColumnIndex(DEALER_ID));
				String notification = cursor.getString(cursor.getColumnIndex(DEALER_NAME));
				String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE_FIELD));
				String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE_FIELD));
				int state=0;
				DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid);
				allAlarmLists.add(alarm);
				cursor.moveToNext();
			}
		}
		return allAlarmLists;
	}*/

	public ArrayList<DealerBean> getAlarm() {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		long insertRowId=0;

		Cursor cursor = db.query(DEALER_DETAILS, null, null, null, null, null, IN_TIME+" ASC");
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				String empid = cursor.getString(cursor.getColumnIndex(EMP_ID));
				String title = cursor.getString(cursor.getColumnIndex(DEALER_ID));
				String notification = cursor.getString(cursor.getColumnIndex(DEALER_NAME));
				String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE_FIELD));
				String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE_FIELD));
				//newly added fields
				String disabled = cursor.getString(cursor.getColumnIndex(DISABLED));
				String inTime = cursor.getString(cursor.getColumnIndex(IN_TIME));
				String outTime = cursor.getString(cursor.getColumnIndex(OUT_TIME));
				String beatPointType=cursor.getString(cursor.getColumnIndex(BEAT_POINT_TYPE));
				String visited=cursor.getString(cursor.getColumnIndex(VISITED));

				boolean inside=false;
				//Cursor cursor2 =db.query(DEALER_VISIT_DETAILS_TABLE_NAME, new String[]{"_id"}, DEALERS_ID +" = ? and "+EXIT_TIME+" IS NULL", new String[]{title}, null, null, null);
				Cursor cursor2 =db.query(DEALER_VISIT_DETAILS_TABLE_NAME, new String[]{"_id"}, DEALERS_ID +" = ? and "+EXIT_TIME+" IS NULL", new String[]{title}, null, null, null);

				if(cursor2.getCount()>0)
				{
					cursor2.moveToFirst();
					insertRowId = cursor2.getInt(cursor2.getColumnIndex(ID_FIELD));
					inside=true;
				}else
				{
					inside= false;
				}
				cursor2.close();

				boolean disabledBoolean=false;

				if(disabled!=null && disabled.trim().equalsIgnoreCase("1"))
				{
					disabledBoolean=true;
				}
				int state=0;
				//DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid);
				//DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid,disabledBoolean,inTime,outTime);
				DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid,disabledBoolean,inTime,outTime,beatPointType);
				alarm.setVisited(visited); // to show visited status for the beat

				alarm.setInside(inside);
				alarm.setInsertRowId(insertRowId);

				allAlarmLists.add(alarm);
				cursor.moveToNext();
			}
		}
		db.close();
		return allAlarmLists;
	}


	public ArrayList<DealerBean> getQRBean(String qrcode) {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		long insertRowId=0;

		Cursor cursor = db.query(DEALER_DETAILS, null,  QR_CODE +" = ?", new String[]{qrcode}, null, null, IN_TIME+" ASC");
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				String empid = cursor.getString(cursor.getColumnIndex(EMP_ID));
				String title = cursor.getString(cursor.getColumnIndex(DEALER_ID));
				String notification = cursor.getString(cursor.getColumnIndex(DEALER_NAME));
				String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE_FIELD));
				String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE_FIELD));
				//newly added fields
				String disabled = cursor.getString(cursor.getColumnIndex(DISABLED));
				String inTime = cursor.getString(cursor.getColumnIndex(IN_TIME));
				String outTime = cursor.getString(cursor.getColumnIndex(OUT_TIME));
				String beatPointType=cursor.getString(cursor.getColumnIndex(BEAT_POINT_TYPE));
				String visited=cursor.getString(cursor.getColumnIndex(VISITED));
				//String qrcode=cursor.getString(cursor.getColumnIndex(QR_CODE));


				int state=0;
				boolean disabledBoolean=false;

				DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid,disabledBoolean,inTime,outTime,beatPointType);
			//	alarm.setVisited(visited); // to show visited status for the beat


				alarm.setInsertRowId(insertRowId);

				allAlarmLists.add(alarm);
				cursor.moveToNext();
			}
		}
		db.close();
		return allAlarmLists;
	}

	public ArrayList<DealerBean> getNearstQRBean(double lat, double lng) {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		long insertRowId=0;

		Cursor cursor = db.query(DEALER_DETAILS, null,  BEAT_POINT_TYPE +" = ?", new String[]{"QR"}, null, null, IN_TIME+" ASC");
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				String empid = cursor.getString(cursor.getColumnIndex(EMP_ID));
				String title = cursor.getString(cursor.getColumnIndex(DEALER_ID));
				String notification = cursor.getString(cursor.getColumnIndex(DEALER_NAME));
				String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE_FIELD));
				String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE_FIELD));
				//newly added fields
				String disabled = cursor.getString(cursor.getColumnIndex(DISABLED));
				String inTime = cursor.getString(cursor.getColumnIndex(IN_TIME));
				String outTime = cursor.getString(cursor.getColumnIndex(OUT_TIME));
				String beatPointType=cursor.getString(cursor.getColumnIndex(BEAT_POINT_TYPE));
				String visited=cursor.getString(cursor.getColumnIndex(VISITED));
				String qrcode=cursor.getString(cursor.getColumnIndex(QR_CODE));


				int state=0;
				boolean disabledBoolean=false;
				double distance=findDistance(lat,lng,Double.parseDouble(latitude),Double.parseDouble(longitude));

				if(distance*1000 <20)
				{
					DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid,disabledBoolean,inTime,outTime,beatPointType);
					//	alarm.setVisited(visited); // to show visited status for the beat


					alarm.setInsertRowId(insertRowId);
					alarm.setQrcode(qrcode);
					allAlarmLists.add(alarm);
				}

				cursor.moveToNext();
			}
		}
		db.close();
		return allAlarmLists;
	}


	public ArrayList<DealerBean> getNearestBeatsAndQRCodes(double lat, double lng) {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		long insertRowId=0;

		Cursor cursor = db.query(DEALER_DETAILS, null,  null,null,  null, null, IN_TIME+" ASC");
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				String empid = cursor.getString(cursor.getColumnIndex(EMP_ID));
				String title = cursor.getString(cursor.getColumnIndex(DEALER_ID));
				String notification = cursor.getString(cursor.getColumnIndex(DEALER_NAME));
				String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE_FIELD));
				String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE_FIELD));
				//newly added fields
				String disabled = cursor.getString(cursor.getColumnIndex(DISABLED));
				String inTime = cursor.getString(cursor.getColumnIndex(IN_TIME));
				String outTime = cursor.getString(cursor.getColumnIndex(OUT_TIME));
				String beatPointType=cursor.getString(cursor.getColumnIndex(BEAT_POINT_TYPE));
				String visited=cursor.getString(cursor.getColumnIndex(VISITED));
				String qrcode=cursor.getString(cursor.getColumnIndex(QR_CODE));


				int state=0;
				boolean disabledBoolean=false;
				double distance=findDistance(lat,lng,Double.parseDouble(latitude),Double.parseDouble(longitude));

				if(distance*1000 <20)
				{
					DealerBean alarm = new DealerBean(id,title, notification,latitude, longitude, state,empid,disabledBoolean,inTime,outTime,beatPointType);
					//	alarm.setVisited(visited); // to show visited status for the beat


					alarm.setInsertRowId(insertRowId);
					alarm.setQrcode(qrcode);
					allAlarmLists.add(alarm);
				}

				cursor.moveToNext();
			}
		}
		db.close();
		return allAlarmLists;
	}

	private double findDistance(double lat, double lon, double currentlat, double currentlon) {

		double dist = 0.0;
		double deltaLat = Math.toRadians(currentlat - lat);
		double deltaLon = Math.toRadians(currentlon - lon);
		lat = Math.toRadians(lat);
		currentlat = Math.toRadians(currentlat);
		lon = Math.toRadians(lon);
		currentlon = Math.toRadians(currentlon);
		double earthRadius = 6371;
		double a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
				Math.cos(lat) * Math.cos(currentlat) * Math.sin(deltaLon/2) * Math.sin(deltaLon/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		dist = earthRadius * c;
		return dist;

	}




	/*public boolean getInside(String dealerId)
	{
		//SQLiteDatabase db = this.getReadableDatabase();


		if(cursor.getCount()>0)
		{
			return true;
		}else
		{
			return false;
		}

	}*/


	public ArrayList<DealerBean> getAllVisitDetails() {

		ArrayList<DealerBean> allAlarmLists = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		SimpleDateFormat dPattern = new SimpleDateFormat("yyyy-MM-dd");

		//Cursor cursor = db.query(DEALER_VISIT_DETAILS_TABLE_NAME, null, "date='"+dPattern.format(new Date())+"'", null, null, null, null);
		Cursor cursor = db.query(DEALER_VISIT_DETAILS_TABLE_NAME, null, "date='"+dPattern.format(new Date())+"'", null, null, null, null);


		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {				
				Date date = null;
				Date inTime = null;
				Date outTime = null;
				
				int id = cursor.getInt(cursor.getColumnIndex(ID));
				String date1 = cursor.getString(cursor.getColumnIndex(DATE));
				String title = cursor.getString(cursor.getColumnIndex(DEALERS_ID));
				String empid = cursor.getString(cursor.getColumnIndex(EMPID));
				String intime = cursor.getString(cursor.getColumnIndex(ENTRY_TIME));
				String outtime = cursor.getString(cursor.getColumnIndex(EXIT_TIME));
				int state=0;
				datePattern = new SimpleDateFormat("yyyy-MM-dd");
				timePattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				try {
					date = datePattern.parse(date1);

					try{

						if(intime!=null){
							inTime = timePattern.parse(intime);					
						}

						// added by dinesh to avoid null point entries at the time of sending the coordinates
						if(intime!=null && outtime==null)
						{
							outtime=intime;
						}
						//end of added code...


						if(outtime!=null){
							outTime = timePattern.parse(outtime); 
						}						
						if(inTime!=null && outTime!=null){						
							timespent =(outTime.getTime())-(inTime.getTime());		
							timespent= timespent/(60000); 
						}else{						
							timespent = 0;
						}
					}catch(Exception e){
						e.printStackTrace();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				if(outtime!=null){
					DealerBean alarm = new DealerBean(id,date, title, empid, inTime, outTime, timespent);
					if("QR".equalsIgnoreCase(alarm.getBeatPointType()))
					{
						alarm.setQrcode(cursor.getString(cursor.getColumnIndex(QR_CODE)));
					}
					allAlarmLists.add(alarm);

				}else{
					DealerBean alarm = new DealerBean(id,date, title, empid, inTime, null, timespent);
					if("QR".equalsIgnoreCase(alarm.getBeatPointType()))
					{
						alarm.setQrcode(cursor.getString(cursor.getColumnIndex(QR_CODE)));
					}
					allAlarmLists.add(alarm);

				}

				cursor.moveToNext();
			}
		}
		return allAlarmLists;
	}	

	public ArrayList<DealerBean> getId(){
		ArrayList<DealerBean> allAlarmId = new ArrayList<DealerBean>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(DEALER_DETAILS, null, null, null, null, null, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			for (int i = 0; i < cursor.getCount(); i++) {
				int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
				DealerBean alarm = new DealerBean(id);
				allAlarmId.add(alarm);
				cursor.moveToNext();
			}
		}
		return allAlarmId;
	}
	public void delete(long id) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(DEALER_DETAILS, ID_FIELD + "=?",new String[] { String.valueOf(id) });
		db.close();
	}

	public void allDelete() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(DEALER_DETAILS, null, null);
		db.close();
	}

	public void deleteVisitDetails() {
		SimpleDateFormat dPattern = new SimpleDateFormat("yyyy-MM-dd");
		SQLiteDatabase db = this.getReadableDatabase();
		String date = dPattern.format(new Date());
		db.delete(DEALER_VISIT_DETAILS_TABLE_NAME, null , null);
		db.close();
	}

	public void updateState(int state, int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(STATE_FIELD, state);
		db.update(DEALER_DETAILS, values, ID_FIELD + "=?",new String[] { String.valueOf(id) });
	}


}
