package com.quad.track;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by My Documents on 3/14/2017.
 */


public class BeatPointListAdapter extends BaseAdapter{

    ArrayList<DealerBean> itemList;

    public Activity context;
    public LayoutInflater inflater;

    public BeatPointListAdapter(Activity context,ArrayList<DealerBean> itemList) {
        super();

        this.context = context;
        this.itemList = itemList;

        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder
    {
        ImageView imgViewLogo;
        TextView txtViewTitle;
        TextView txtViewTimings;
        ImageView imgPictureIcom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder;
        if(convertView==null)
        {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.rowlayoutnew, null);

            holder.imgViewLogo = (ImageView) convertView.findViewById(R.id.icon);
            holder.txtViewTitle = (TextView) convertView.findViewById(R.id.label);
            holder.txtViewTimings = (TextView) convertView.findViewById(R.id.timings);
            holder.imgPictureIcom=(ImageView) convertView.findViewById(R.id.takepictureforattendance);



            convertView.setTag(holder);
        }
        else
        {
            holder=(ViewHolder)convertView.getTag();
        }

       DealerBean bean = (DealerBean) itemList.get(position);

       if("BP".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.beatpoints);
       }else if("BC".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.bc);
       }else if("QR".equalsIgnoreCase(bean.getBeatPointType()))
       {
           holder.imgViewLogo.setImageResource(R.drawable.qrcode);
           holder.imgPictureIcom.setImageResource(R.drawable.qr);
       }
        try{

            SimpleDateFormat sf=new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat newSf=new SimpleDateFormat("hh:mm a");

            Date inTime=sf.parse(bean.getDefinedInTime());
            Date outTime=sf.parse(bean.getDefinedOutTime());
           if("Y".equalsIgnoreCase(bean.getVisited()))
           {
               holder.txtViewTitle.setTextColor(Color.parseColor("#000000"));
               holder.txtViewTimings.setTextColor(Color.parseColor("#000000"));

               holder.txtViewTitle.setBackgroundColor(Color.parseColor("#80ff80"));
               holder.txtViewTimings.setBackgroundColor(Color.parseColor("#80ff80"));
               convertView.setBackgroundColor(Color.parseColor("#80ff80"));
           }else
           {
               holder.txtViewTitle.setTextColor(Color.parseColor("#f46242"));
               holder.txtViewTimings.setTextColor(Color.parseColor("#ffffff"));
               holder.txtViewTitle.setBackgroundColor(Color.parseColor("#000000"));
               holder.txtViewTimings.setBackgroundColor(Color.parseColor("#000000"));
               convertView.setBackgroundColor(Color.parseColor("#000000"));
           }
            holder.txtViewTitle.setText(bean.getDealerName());
            holder.txtViewTimings.setText(newSf.format(inTime) + "     " +newSf.format(outTime));



        }catch(Exception e)
        {
            e.printStackTrace();
        }


        return convertView;
    }

}