package com.quad.track;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by My Documents on 2/6/2017.
 */

public class TimeChangedReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {

        DatabaseHandler databaseHandler = new DatabaseHandler(context);
        try {
           /* SharedPreferences prefs = context.getSharedPreferences("datastore",
                    Context.MODE_WORLD_READABLE);

           String deviceid = data.getString("deviceid", null);
*/

           if(0==Settings.System.getInt(context.getContentResolver(),Settings.System.AUTO_TIME, 0))
            {
                Date date = new Date();
                DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
                String currentDateTimeString = utilDateFormatter.format(date);
                databaseHandler.addData("CHANGED", currentDateTimeString, "TIME");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
