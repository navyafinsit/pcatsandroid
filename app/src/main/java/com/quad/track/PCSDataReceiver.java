package com.quad.track;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

public class PCSDataReceiver extends BroadcastReceiver {
    
	
	
    static long timertime;
    static long distance;
    static long time;
    static String ipaddress;
    static String controlRoomMobileNumber;
    SharedPreferences data;
    SmsManager sms;
	
   
    public void onReceive(Context context, Intent intent) {
       
    	
    	 sms = SmsManager.getDefault();
    	 
    	 System.out.println(" message recieved @@@@@@@  ");

    	 data = context.getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		 SharedPreferences.Editor editor = data.edit();
    	 
		 final Bundle bundle = intent.getExtras();
		 
         String message=null;
         try {             
            if (bundle != null) {                 
                final Object[] pdusObj = (Object[]) bundle.get("pdus");                
               
                 
                for (int i = 0; i < pdusObj.length; i++) {
                     
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    message = currentMessage.getDisplayMessageBody();
                    
                 
                    /*if(message.contains("battery*#@")) {
                    	
                    	 Intent battery= context.getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                         int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                         int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                         float batteryPct = level / (float)scale *100;
                         ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	               		 if (connectivity != null) {
	               			  
                        	 NetworkInfo[] info = connectivity.getAllNetworkInfo();
	               			  
	               			  if (info != null) {
	               				  
	               				  for (int j = 0; j < info.length; i++) {
	               					  
	               					  if (info[j].getState() == NetworkInfo.State.CONNECTED) {
	               						  
	               						 // SEND SMS USING INTERNET
	               						   
	               						   sms.sendTextMessage("9441283028", null, "Battery Level :"+batteryPct+"% Left", null, null);
	               						 
	               						 
	               					  } else {
	               						  
	                                        // SEND SMS USING GSM
		               						
		               						sms.sendTextMessage("9441283028", null, "Battery Level :"+batteryPct+"% Left", null, null);
	               						  
	               					    }                					  
	               					  
	               				  }
	               				  
	               			  }
	                           
	               		   }
                         
                     } 
                    
                    else */
                    
                   /* if(message.contains("gps*#@")) {
                    	   
	                         String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	                         
	                         if(!provider.contains("gps")) {
	                        	 
	                        	 Intent gpsIntent = new Intent("android.location.GPS_ENABLED_CHANGE");
	        			         intent.putExtra("enabled", true);
	        			         context.sendBroadcast(gpsIntent);
	                        	 final Intent poke = new Intent();
		        			     poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
		        			     poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
		        			     poke.setData(Uri.parse("3")); 
		        			     context.sendBroadcast(poke);
		        			     
		        			     Intent startIntent = new Intent();
			        			 startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
			        			 context.stopService(startIntent);
			        			 context.startService(startIntent);  
		        			     
	                         } else {
	                         
			        			 







			        			 
	                         }
	                                       	 
                     }                   
                     else if(message.contains("internet*#@")) {            	 
                   	  
		                  final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
		              	  final Class conmanClass = Class.forName(conman.getClass().getName());
		              	  final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
	 	              	  iConnectivityManagerField.setAccessible(true);
		              	  final Object iConnectivityManager = iConnectivityManagerField.get(conman);
		              	  final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
		              	  final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
		              	  setMobileDataEnabledMethod.setAccessible(true);
		                  setMobileDataEnabledMethod.invoke(iConnectivityManager, true);
                   	 
                   	 }                     
                     else*/
                    
                    
                    
                    if(message.contains("timer@!")) {
                    	 
                    	 String[] timer=message.split("!");
                    	 timertime=Long.parseLong(timer[1]);
                    	 editor.putLong("manualtimer", timertime);
                    	 editor.commit();                    	 
                    	 Intent startIntent = new Intent();
        			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
        			     context.stopService(startIntent);
        				 context.startService(startIntent);    
                    	 	
                     }
                    
                    
                    
                    if(message.contains("distance@!")) {
                   	 
                   	 String[] dist=message.split("!");
                   	 distance=Long.parseLong(dist[1]);
                   	 editor.putLong("distance", distance);
                   	 editor.commit();                    	 
                   	 Intent startIntent = new Intent();
       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
       			     context.stopService(startIntent);
       				 context.startService(startIntent);    
                   	 	
                    }
                    
                    
                    
                    if(message.contains("time@!")) {
                      	 
                      	 String[] timeArr=message.split("!");
                      	 time=Long.parseLong(timeArr[1]);
                      	 editor.putLong("time", time);
                      	 editor.commit();                    	 
                      	 Intent startIntent = new Intent();
          			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
          			     context.stopService(startIntent);
          				 context.startService(startIntent);    
                      	 	
                       }
                    
                   /* if(message.contains("stopservice@!")) {
                     	 
                     	 String[] timeArr=message.split("!");
                     	 String ipAddress=(timeArr[1]);
                     	 editor.putString("ipid", ipAddress);
                     	 editor.commit();                    	 
                     	 Intent startIntent = new Intent();
         			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
         			     context.stopService(startIntent);
         				 context.startService(startIntent);    
                     	 	
                      }*/
                    
                    
                    
                    if(message.contains("ipchange@!")) {
                   	 
	                   	 String[] ipchanger = message.split("!");
	                   	 ipaddress = ipchanger[1];
	                   	 editor.putString("ipid", ipaddress);
	                   	 editor.commit();                    	 
	                   	 Intent startIntent = new Intent();
	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
	       			     context.stopService(startIntent);
	       				 context.startService(startIntent);    
                   	 	
                    }
                    
                    
                    if(message.contains("requestlocation@!")) {		
                      	 
                    	 String[] requestLocation = message.split("!");
                    	 controlRoomMobileNumber = requestLocation[1];
                    	 
                    	 if(controlRoomMobileNumber != null && controlRoomMobileNumber.trim().length() > 0) {
                    		 editor.putString("controlRoomMobileNumber",controlRoomMobileNumber.trim() );
                    		 editor.putBoolean("requestLocationUpdate", true);
                    		 editor.commit();
                    		 Intent startIntent = new Intent();
    	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
    	       			     context.stopService(startIntent);
    	       				 context.startService(startIntent); 
                    	 }
                    	  
                  	 	
                    }
                    
                    if(message.contains("emaillog@!")) {
                     	 
                   		 editor.putBoolean("requestemaillog", true);
                   		 editor.commit();
                   		 Intent startIntent = new Intent();
   	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
   	       			     context.stopService(startIntent);
   	       				 context.startService(startIntent); 
                   		
                   }
                    
                    if(message.contains("stopservice@!")) {
                    	 
                  		 editor.putBoolean("stopservice", true);
                  		 editor.commit();
                  		 Intent startIntent = new Intent();
  	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
  	       			     context.stopService(startIntent);
  	       				
                  		
                   }
                    
                    if(message.contains("restartservice@!")) {
                   	 
                 		 Intent startIntent = new Intent();
 	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
	 	       			 editor.putBoolean("stopservice", false);
	              		 editor.commit();
 	       			     context.stopService(startIntent);
 	       				 context.startService(startIntent); 
                 		
                  }
                    
                  if(message.contains("deletebackupdata@!")) {
                      	 
                		 editor.putBoolean("deletebackupdata", true);
                		 editor.commit();
                		 Intent startIntent = new Intent();
	       			     startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
	       			     context.stopService(startIntent);
	       				 context.startService(startIntent); 
                		
                 }
                    
                  
                   
                  
                    	     
                } // end for loop
              } // bundle is null
 
        } catch (Exception e) {
           
             
        }
        
        if(message.contains("timer@!") || message.contains("distance@!") || message.contains("time@!") ||  message.contains("ipchange@!") || message.contains("requestlocation@!") || message.contains("emaillog@!") || message.contains("stopservice@!") || message.contains("restartservice@!") || message.contains("deletebackupdata@!") ){
         
        		abortBroadcast();
        }
        
        
        
    } 
    
    
    
    
}



