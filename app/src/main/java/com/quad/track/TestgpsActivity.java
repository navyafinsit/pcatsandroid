package com.quad.track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;


public class TestgpsActivity extends Activity {
	ProgressDialog serverProgress;
	private long distance = 30; // in Meters
	private long time = 0; // in Milliseconds
	private String textData;
    private String deviceid = null;
    private String ipid = null;
    long manualPollUpdatesTime = 300000;
    private String outputData = "";
    private TextView errorText,menuTextView,errortextphone;

    private Button saveBtn,cancelBtn;
    private Button updateBtn;
    boolean isGPSEnabled = false;
    Button reconnectButton,logEmailButton,deleteButton;
	// flag for network status
	boolean isNetworkEnabled = false;
    private static final int START_GPS = 1;
    private static final int STOP_GPS = 2;
    private static final int MODIFY_DATA=3;
    String fileName = "errordata.txt";  
    File root = Environment.getExternalStorageDirectory();
    boolean isValidated = false;
    SharedPreferences data;
    boolean availableFlag = false;
    LocationManager locationManager; 
    String configDistance = "";
	String phoneNumber="";
    String configTime = "";
    String configDevice = "";
    String configIP = "";
    AlertDialog ad = null;
    
    
    
    final class DeviceConfigureTask extends AsyncTask<String, Void, String> {
		
    	
  	 
  		@Override
  		protected void onPreExecute() {
  			// TODO Auto-generated method stub
  			super.onPreExecute();
  			serverProgress.show();	
  		}
  		
          @Override
          protected String doInBackground(String... urls) {
              String output = null;
              for (String url : urls) {
                  output = getOutputFromUrl(url);
                 
              }
              return output;
          }   
          private String getOutputFromUrl(String url) {
              String output = "";



              try {
          		    String urlStr = url.toString().replace(" ", "%20");
            		URL urlData = new URL(urlStr);
            		URLConnection yc = urlData.openConnection();
            		yc.setConnectTimeout(95000);
            		
            		BufferedReader in = new BufferedReader(new InputStreamReader( yc.getInputStream()));
            		String inputResult = "";
                    String inputLine = "";
        			while ((inputLine = in.readLine()) != null) {
        				inputResult = inputResult + inputLine;
        			   
        			}
        			
        			
        			
        			in.close();
                
            	  
               
        			
        			output = inputResult;
        			
              } catch (Exception e) {
              	output = "";
              	output = "INTERNETPROBLEM";
               
                  
              }
             
              return output;
          }
   
          @Override
          protected void onPostExecute(String output) {
        	  
          	 try {
				serverProgress.cancel();
				 if(output.contains("FALSE")) { 
					alertboxToConfigure("DUPLICATE DEVICE ID",  "Device Id already exists");
					availableFlag = false;
				} else if(output.contains("TRUE")){
					availableFlag = true;
				} else if(output.contains("NONEXIST")){
					alertboxToConfigure("WRONG DEVICE ID",  "Device Id not found in Database");
					availableFlag = false;
				} else if(output.contains("NOTAUTHORIZED")){
					 alertboxToConfigure("DEVICE NOT AUTHORIZED",  "Device Id not AUTHORIZED");
					 availableFlag = false;
				 }

				 if(availableFlag) {
						
						//if(configDistance.equals("")) {
							configDistance = "30";
						//}
						
						if(configTime.equals("")) {
							configTime = "0";
						}						
						if(configIP.equals("")) {
							configIP = IPInfo.IPADDRESS;
						}
					
						
						
						ad.cancel();
						   
						SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
						SharedPreferences.Editor editor = data.edit();
						
						editor.putString("deviceid", configDevice);
						editor.putString("ipid", configIP);
						editor.putLong("distance", Long.parseLong(configDistance));
						editor.putLong("time", Long.parseLong(configTime));
						editor.putLong("manualtimer", 300000);
						editor.putString("pass", "demo"); 

					    //SET TRACK POINT COUNTER HERE..//added by dinesh for setting a counter for trackpoint..
					 	//editor.putLong("counter",0);

						editor.commit();
						alertboxToConfigure("CONFIGURATION SUCCESS",  "Device Configured Successfully");
						
						try {
							DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
						} catch(Exception e) {
							e.printStackTrace();
						}
						
						
						
						/*// newly added code 5 jan
						File file1 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
										configDevice);
						File file2 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
										configDevice+"-RS");
					*//*	File file3 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
										configDevice+"-ROWDY");
						File file4 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
						
										
										configDevice+"-SUSPECT");*//*
						
						if(!file1.mkdirs()) {
						    file1.mkdir();
						}
						if(!file2.mkdirs()) {
							file2.mkdir();
						}

						*//*if(!file3.mkdirs()) {
						    file3.mkdir();
						}
						if(!file4.mkdirs()) {
							file4.mkdir();
						}	*//*
						*/
				    }
	          	 
				 
				 
				
			} catch (Exception e) {
				
			}
          	 
          	 
          	 
   			
   			
  				
  			}
  	    }
    
    boolean isMailSent = false;
    
    
    final class MailTask extends AsyncTask<String, Void, String> {
		
  		@Override
  		protected void onPreExecute() {
  			super.onPreExecute();
  			isMailSent = false;
  			serverProgress.setMessage("Emailing Log Report...");
  			serverProgress.show();	
  		}
  		
          @Override
          protected String doInBackground(String... urls) {
             try {
            	 	DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
        			SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
        			String deviceId = data.getString("deviceid", null);       				
        			String text = dbHandler.getAlertData(deviceId);
        			//System.out.println(text.length());
        			if(text.length() > 0) {			    
	        	        GMailSender sender = new GMailSender("quadriviumtracking@gmail.com", "quad@123");
	        	        sender.sendMail("GPS AND INTERNET REPORTS OF "+deviceId,   
	        	                		 text,   
	        	                         "quadriviumtracking@gmail.com","quadriviumbeats@gmail.com");   
	        	        dbHandler.deleteData();
	        	        return "SUCCESS";
	        	        
        			} else {
        				return "NOLOG";
        			}
        	       
             } catch(Exception e) {
            	 
            	 return "EXCEPTION"; 
             }
        	  
          }   
          
   
          @Override
          protected void onPostExecute(String output) {
        	  
	          	try {
	          		
	          		 serverProgress.cancel();
	          		
	          		 if("SUCCESS".equals(output)) 
	          			alertboxToConfigureEmail("EMAIL STATUS",  "Log Reports mailed");
	          		 
	          		 if("NOLOG".equals(output))
	          			alertboxToConfigureEmail("EMAIL STATUS",  "No Log Reports exist");
	          		 
	          		 if("EXCEPTION".equals(output))
	          			alertboxToConfigureEmail("EMAIL STATUS",  "Problem in sending email. Please try again later.");
	          	 
				} catch (Exception e) {
					
				}
          	 
          }
  	    }
    
    
    
    
  	

    public void setMobileDataEnabled(Context context, boolean enabled) throws Exception{
    	  final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	  final Class conmanClass = Class.forName(conman.getClass().getName());
    	  final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
    	  iConnectivityManagerField.setAccessible(true);
    	  final Object iConnectivityManager = iConnectivityManagerField.get(conman);
    	  final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
    	  final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
    	  setMobileDataEnabledMethod.setAccessible(true);

    	  setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
    	} 
    
    
    
    
    public void alert() {
		 
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestgpsActivity.this);

		// set title
		alertDialogBuilder.setTitle("Delete Backup Data");

		// set dialog message
		alertDialogBuilder
			.setMessage("You are about to delete backup data. If any backup data persists it gets deleted.")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					try {
						DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
						dbHandler.delete();
						Toast.makeText(getApplicationContext(), "Backup data deleted successfully", Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getApplicationContext(), "Backup data not sent", Toast.LENGTH_LONG).show();
					}
					
				}
			  })
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			
			alertDialog.show();
			
		}
    
    
    
    
    public void alertEmail() {
		 
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestgpsActivity.this);

		// set title
		alertDialogBuilder.setTitle("Email Log Report");

		// set dialog message
		alertDialogBuilder
			.setMessage("You are about to mail GPS and Internet Logs. Do you really want to continue.")
			.setCancelable(false)
			.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					try {
						
						ConnectionDetector connectionDetector=new ConnectionDetector(TestgpsActivity.this);
	        			
						if(connectionDetector.isConnectingToInternet()) {
		        			MailTask mailTask = new MailTask();
							mailTask.execute("MAIL");
							
						} else {
	        				Toast.makeText(getApplicationContext(), "Please Enable Internet !", Toast.LENGTH_LONG).show();
	        				dialog.cancel();
	        			}
						
					} catch (Exception e) {
						
					}
					
				}
			  })
			.setNegativeButton("No",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					
					dialog.cancel();
				}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			
			alertDialog.show();
			
		}

     
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main); 
		menuTextView=(TextView)findViewById(R.id.menutext);
	    reconnectButton=(Button)findViewById(R.id.reconnect);
	    logEmailButton=(Button)findViewById(R.id.enable);
	    deleteButton=(Button)findViewById(R.id.deletebackup);
	    
	    if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

		logEmailButton.setEnabled(false);
		deleteButton.setEnabled(false);
	    
	    
	    reconnectButton.setOnClickListener(new View.OnClickListener() {
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String iptext = "";
				String distancetext = "";
				String timetext = "";
				
				SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				
				SharedPreferences.Editor editor = data.edit();
				
				if(!iptext.equals("")) {
					editor.putString("ipid", iptext);
				}
				
				if(!distancetext.equals("")) {
					editor.putLong("distance", Long.parseLong(distancetext));
				}
				
				if(!timetext.equals("")) {
					editor.putLong("time", Long.parseLong(timetext));
				}
				
				
				editor.commit();
				stopService(new Intent(TestgpsActivity.this,GPSService.class)); //
				stopService(new Intent(TestgpsActivity.this,EmpTrackingService.class)); //   pfence

				data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				deviceid = data.getString("deviceid", null);
				ipid = data.getString("ipid", IPInfo.IPADDRESS);
				distance = data.getLong("distance",30);
				time = data.getLong("time",0);
				manualPollUpdatesTime = data.getLong("manualtimer",300000);
				
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

				///boolean disableDevice=data.getString("disabled")
					if (!isGPSEnabled) {
						showSettingsAlert();
					} else {
					
				
						Intent gpsService = new Intent(TestgpsActivity.this,GPSService.class);
						//PFENCE intent
						Intent fenceService = new Intent(TestgpsActivity.this,EmpTrackingService.class);
						Bundle b = new Bundle();
							b.putString("deviceid", deviceid);
							b.putString("ipid", ipid);
							b.putLong("distance", distance);
							b.putLong("time", time);
							b.putLong("manualtimer", manualPollUpdatesTime);
							gpsService.putExtras(b);
							startService(gpsService);
							//start PFence ServiceToo
							startService(fenceService);    // added at pfence integration
							Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show();
							finish();
				
					}
				
				
				
			}
		});
	    
	    logEmailButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					alertEmail();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
	    
	    
	    deleteButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				try {
					alert();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
	    
	    
	    serverProgress = new ProgressDialog(TestgpsActivity.this);
		serverProgress.setMessage("Configuring Device. Please wait");
		serverProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		SharedPreferences dataPreference = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		textData = dataPreference.getString("deviceid", null);  
		
		configDistance = "";
		configTime = "";
		configDevice = "";
		configIP = "";
		
		if(textData == null) { //which means this is the first time PCATS IS ACTIVATED // wheren there is not deviceid in the sharedpreference
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			final View layout = inflater.inflate(R.layout.entrydialog,(ViewGroup) findViewById(R.id.layout_root));
			
			final AlertDialog.Builder adb = new AlertDialog.Builder(TestgpsActivity.this);
			adb.setTitle("Configuration Dialog");
			adb.setView(layout);
			adb.setCancelable(false);
			ad = adb.create();
			ad.show();
			
			/*deviceEditText = (EditText) layout.findViewById(R.id.deviceid);
			ipEditText = (EditText) layout.findViewById(R.id.ipid);*/
			saveBtn = (Button) layout.findViewById(R.id.save);
			cancelBtn=(Button) layout.findViewById(R.id.cancel);
			//saveBtn.setEnabled(false);
			errorText = (TextView) layout.findViewById(R.id.errortext);
			errortextphone=(TextView) layout.findViewById(R.id.errortextphone);
		
			saveBtn.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ConnectionDetector connectionDetector = new ConnectionDetector(TestgpsActivity.this);
					if(connectionDetector.isConnectingToInternet()) {
						configDistance ="";
						phoneNumber=((EditText)layout.findViewById(R.id.distance)).getText().toString().trim();
						configTime = ((EditText)layout.findViewById(R.id.time)).getText().toString().trim();
						configDevice = ((EditText)layout.findViewById(R.id.deviceid)).getText().toString().trim();
						configIP = ((EditText)layout.findViewById(R.id.ipid)).getText().toString().trim();
						if(configIP.equals("")) {
							configIP = IPInfo.IPADDRESS;
						}
						
						if(configDevice.equals("") || phoneNumber==null || phoneNumber =="" || phoneNumber.length()!=10  )
						{
							if(configDevice.equals(""))
							{
								errorText.setText("Please Provide Device Id");
							}

							if(phoneNumber!=null && phoneNumber !="" && phoneNumber.length()!=10)
							{
								errortextphone.setText("Enter valid mobile number ");
							}

						} else {

								TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
								String IMEINumber = mngr.getDeviceId();

							   // String urlStrDevice = "http://"+configIP+"/vehicleservice/device?devicename="+configDevice;
							    String urlStrDevice = "http://"+configIP+"/vehicleservice/device?devicename="+configDevice+"&imei="+IMEINumber+"&phone="+phoneNumber;
							    try{
							         urlStrDevice.replaceAll(" ", "%20");
									 DeviceConfigureTask deviceConfigureTask = new   DeviceConfigureTask();
							         
							         
							         deviceConfigureTask .execute(urlStrDevice);
								  } catch (Exception e) {
									  Toast.makeText(getApplicationContext(), "Problem with Internet Connection", Toast.LENGTH_LONG).show();
								   }
							      
							}
						
					
				}
					else
					{
						alertboxToLogin("Internet Connectivity Failed", "Make sure your device has Internet Connection");
					}
				
				}
			});
			
			cancelBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ad.cancel();
					finish();
					
				}
			});
		
		    
		} else { // everytime we enter the password...
			try {
				
				
				data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				
				final String password = data.getString("pass", null);
				String deviceIdentity = data.getString("deviceid", null);
				AlertDialog.Builder alert = new AlertDialog.Builder(this);

				alert.setTitle("Device Already Configured!");
				alert.setMessage("Device ID "+deviceIdentity);

				// Set an EditText view to get user input 
				final EditText input = new EditText(this);
				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				//alert.setView(input);
                alert.setCancelable(false);
		/*		alert.setPositiveButton("Unlock", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				 *//*   reconnectButton.setVisibility(View.VISIBLE);
				    logEmailButton.setVisibility(View.VISIBLE);
				    deleteButton.setVisibility(View.VISIBLE);
				    menuTextView.setVisibility(View.VISIBLE);
					String value = input.getText().toString();
					if(value != null) {
						value = value.trim();
						
						if(value.equals(password)) {
							deviceid = data.getString("deviceid", null);
							ipid = data.getString("ipid", IPInfo.IPADDRESS);
							distance = data.getLong("distance",30);
							time = data.getLong("time",0);
							manualPollUpdatesTime = data.getLong("manualtimer", 300000);
							
							
							
							SharedPreferences.Editor editor = data.edit();
							
							editor.putString("deviceid", deviceid);
							editor.putString("ipid", ipid);
							editor.putLong("distance", distance);
							editor.putLong("time", time);
							editor.putLong("manualtimer", manualPollUpdatesTime);
							editor.commit();
						
						}else {
							Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_LONG).show();
							finish();
						}
					}
				 *//*
				  }
				});*/
				alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					  
			    }
					});

				alert.show();
				
				
			}catch (Exception e) {
				
				
				try {
					FileOutputStream f = new FileOutputStream(new File(root, fileName),true);
					StackTraceElement s[] = e.getStackTrace();
					Calendar c = Calendar.getInstance(); 
					String writeCard ="*********"+ c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND)+"  ";
					for(int i=0;i<s.length;i++) {
						String errorData = s[i].toString();
						if(errorData.contains("com.quad.vehicle")) {
							writeCard = writeCard + errorData;
							break;
						}
					}
					writeCard = writeCard+"  "+e.getClass()+":"+e.getMessage();
					
					byte b[] = new byte[writeCard.length()];
					b = writeCard.getBytes();
					f.write(b);		
					
				} catch (Exception e1) { 
					
				}
			}
			
		}
	
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
	/*	MenuItem startGPS = menu.add(0, START_GPS, 1, "Start Tracking");
		MenuItem stopGPS = menu.add(0,STOP_GPS,2,"Stop Tracking");
		MenuItem modifyData = menu.add(0,MODIFY_DATA,3,"Edit");
		
		if(deviceid == null) {
			startGPS.setEnabled(false);
			stopGPS.setEnabled(false);
		} else {
			startGPS.setEnabled(true);
			stopGPS.setEnabled(true);
		}*/
		
		return super.onCreateOptionsMenu(menu);
	}
	
	
	public void alertboxToLogin(String title, String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		    		  new DialogInterface.OnClickListener() {
		    	  public void onClick(DialogInterface dialog, int whichButton){
		               	
		         }
		         }).show();
		}
	}	
	
	
	
	public void alertboxToConfigure(String title, String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		    		  new DialogInterface.OnClickListener() {
		    	  public void onClick(DialogInterface dialog, int whichButton){
		               finish();	
		         }
		         }).show();
		}
	}	
	
	
	public void alertboxToConfigureEmail(String title, String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		    		  new DialogInterface.OnClickListener() {
		    	  public void onClick(DialogInterface dialog, int whichButton){
		               	
		         }
		         }).show();
		}
	}	

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		
		
		return super.onPrepareOptionsMenu(menu);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		try {
		 int selectedItem = item.getItemId();
		 
		 switch(selectedItem) {
		 
		 	case START_GPS:  	
		 		
		 		                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

								
								if (!isGPSEnabled) {
									showSettingsAlert();
								} else {
								
			 		                 Intent gpsService = new Intent(TestgpsActivity.this,GPSService.class);
									 Intent fenceService = new Intent(TestgpsActivity.this,EmpTrackingService.class);
				 					 Bundle b = new Bundle();
				 					 b.putString("deviceid", deviceid);
				 					 b.putString("ipid", ipid);
				 					 b.putLong("distance", distance);
				 					 b.putLong("time", time);
				 					 b.putLong("manualtimer", manualPollUpdatesTime);
				 					 gpsService.putExtras(b);
				 					 startService(gpsService);
									 startService(fenceService); //added at pfence integration
				 					 Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show();
				 					 finish();
								}
		 		                
			 					 break;
			 
		 	case STOP_GPS:   	 
		 		                 
		 		                 stopService(new Intent(TestgpsActivity.this,GPSService.class));
					             stopService(new Intent(TestgpsActivity.this,EmpTrackingService.class)); //   pfence
		 		                 Toast.makeText(getApplicationContext(), "Tracking Stopped", Toast.LENGTH_LONG).show(); 
		 						 finish();
		 					 	 break;
		 					 
		 					 	 
		 	case MODIFY_DATA: 	 try {
		 						 LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								 final View layout = inflater.inflate(R.layout.editdialog,(ViewGroup) findViewById(R.id.layout_editroot));
								
								 final AlertDialog.Builder adb = new AlertDialog.Builder(TestgpsActivity.this);
								 adb.setTitle("Update Configuration Data");
								 adb.setView(layout);
								 adb.setCancelable(false);
								 final AlertDialog ad = adb.create();
								 ad.show();
								
								 updateBtn = (Button) layout.findViewById(R.id.update);
								 cancelBtn=(Button)   layout.findViewById(R.id.cancel);
								 cancelBtn.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										ad.cancel();
										finish();
										
									}
								});
								 updateBtn.setOnClickListener(new View.OnClickListener() {
									
									public void onClick(View v) {
										// TODO Auto-generated method stub
										
										String iptext = ((EditText)layout.findViewById(R.id.ipedit)).getText().toString().trim();
										String distancetext = ((EditText)layout.findViewById(R.id.distanceedit)).getText().toString().trim();
										String timetext = ((EditText)layout.findViewById(R.id.timeedit)).getText().toString().trim();
										
										SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
										
										SharedPreferences.Editor editor = data.edit();
										
										if(!iptext.equals("")) {
											editor.putString("ipid", iptext);
										}
										
										if(!distancetext.equals("")) {
											editor.putLong("distance", Long.parseLong(distancetext));
										}
										
										if(!timetext.equals("")) {
											editor.putLong("time", Long.parseLong(timetext));
										}
										
										
										editor.commit();
										stopService(new Intent(TestgpsActivity.this,GPSService.class));
										stopService(new Intent(TestgpsActivity.this,EmpTrackingService.class)); //   pfence
										data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
										deviceid = data.getString("deviceid", null);
										ipid = data.getString("ipid", IPInfo.IPADDRESS);
										distance = data.getLong("distance",30);
										time = data.getLong("time",0);
										manualPollUpdatesTime = data.getLong("manualtimer",300000);
										
										
			                            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

											
											if (!isGPSEnabled) {
												showSettingsAlert();
											} else {
											
										
										Intent gpsService = new Intent(TestgpsActivity.this,GPSService.class);
										Intent fenceService = new Intent(TestgpsActivity.this,EmpTrackingService.class);
										Bundle b = new Bundle();
					 					b.putString("deviceid", deviceid);
					 					b.putString("ipid", ipid);
					 					b.putLong("distance", distance);
					 					b.putLong("time", time);
					 					b.putLong("manualtimer", manualPollUpdatesTime);
					 					gpsService.putExtras(b);
					 					
					 					startService(gpsService);
										startService(fenceService);   // started at pfence integration time.

					 					Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show(); 
					 					 ad.cancel();
				 					     finish();
											}
									}
								});
								 
								 
		 					 }catch (Exception e) {
								
							}
		 
		 }
		}catch (Exception e) {
			try {
				FileOutputStream f = new FileOutputStream(new File(root, fileName),true);
				StackTraceElement s[] = e.getStackTrace();
				Calendar c = Calendar.getInstance(); 
				String writeCard ="*********"+ c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND)+"  ";
				for(int i=0;i<s.length;i++) {
					String errorData = s[i].toString();
					if(errorData.contains("com.quad.vehicle")) {
						writeCard = writeCard + errorData;
						break;
					}
				}
				writeCard = writeCard+"  "+e.getClass()+":"+e.getMessage();
				
				byte b[] = new byte[writeCard.length()];
				b = writeCard.getBytes();
				f.write(b);		
				
			} catch (Exception e1) { 
				
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}



	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	
	
	
	
	
	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
   	 
        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");
 
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Go to settings menu?");
        alertDialog.setCancelable(false);
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            	startActivity(intent);
            	
            }
        });
 
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
            }
        });
 
        // Showing Alert Message
        AlertDialog alertDialog1=alertDialog.create();
        alertDialog1.show();
	}


}
