package com.quad.track;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LocationActivity extends FragmentActivity implements
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "LocationActivity";
  /*  private static final long INTERVAL = 1000 * 60 * 1; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 60 * 1; // 1 minute*/

    private static final long INTERVAL = 1000 * 5 * 1; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 5 * 1; // 1 minute

    Button btnFusedLocation;
    TextView tvLocation;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    String mLastUpdateTime;
    GoogleMap googleMap;
    ArrayList<DealerBean> beatPointsList;
    Marker currentLocationMarker;
    boolean firstime=true;

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...............................");
        //show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        beatPointsList= (ArrayList<DealerBean>)getIntent().getExtras().getSerializable("beatPoints");

        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        setContentView(R.layout.activity_location_google_map);
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        googleMap = fm.getMap();
        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop fired ..............");
        mGoogleApiClient.disconnect();
        Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Firing onLocationChanged..............................................");
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        addMarker();
        addBeatPoints();
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();

        // following four lines requires 'Google Maps Android API Utility Library'
        // https://developers.google.com/maps/documentation/android/utility/
        // I have used this to display the time as title for location markers
        // you can safely comment the following four lines but for this info
        IconGenerator iconFactory = new IconGenerator(this);
        iconFactory.setStyle(IconGenerator.STYLE_BLUE);
        options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon("YOU")));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng);
        if(currentLocationMarker==null)
        {
            currentLocationMarker = googleMap.addMarker(options);
            long atTime = mCurrentLocation.getTime();
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date(atTime));
            currentLocationMarker.setTitle("YOU ARE HERE");
        }else // we need to animate to new position
        {
            currentLocationMarker.setPosition(currentLatLng);
        }

        Log.d(TAG, "Marker added.............................");
        if(firstime)
        {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,13));
            firstime=false;
        }
        Log.d(TAG, "Zoom done.............................");
    }


    private void addBeatPoints() {




        // following four lines requires 'Google Maps Android API Utility Library'
        // https://developers.google.com/maps/documentation/android/utility/
        // I have used this to display the time as title for location markers
        // you can safely comment the following four lines but for this info

      for(DealerBean db:beatPointsList)
      {
          MarkerOptions options = new MarkerOptions();
         /* IconGenerator iconFactory = new IconGenerator(this);
          iconFactory.setStyle(IconGenerator.STYLE_RED);*/
         // options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(db.getDealerName())));

          IconGenerator iconFactory = new IconGenerator(this);
          iconFactory.setStyle(IconGenerator.STYLE_DEFAULT);
          ImageView im=new ImageView(this);


          if("QR".equalsIgnoreCase(db.getBeatPointType()))
          {
              //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.qr));

              im.setImageResource(R.drawable.qc32);


          }else if("BP".equalsIgnoreCase(db.getBeatPointType()))
          {

              //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.flag32));
              //options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon()));
              im.setImageResource(R.drawable.flag32);

          }else if("BC".equalsIgnoreCase(db.getBeatPointType()))
          {
              //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.bc));
              im.setImageResource(R.drawable.bc32);

          }

          iconFactory.setContentView(im);
          options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon()));


          options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

          LatLng currentLatLng = new LatLng(Double.parseDouble(db.getLatitude()), Double.parseDouble(db.getLongitude()));
          options.position(currentLatLng);
          Marker mapMarker = googleMap.addMarker(options);

          googleMap.addCircle(new CircleOptions()
                  .center(currentLatLng)
                  .radius(50)
                  .strokeColor(Color.RED)
                  .fillColor(0x220000FF)
                  .strokeWidth(5)
          );


         // long atTime = mCurrentLocation.getTime();
         // mLastUpdateTime = DateFormat.getTimeInstance().format(new Date(atTime));
          mapMarker.setTitle(db.getDealerName());
          Log.d(TAG, "Marker added.............................");
         // googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,13));
          Log.d(TAG, "Zoom done.............................");
      }

    }
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            Log.d(TAG, "Location update resumed .....................");
        }
    }
}