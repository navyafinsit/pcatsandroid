package com.quad.track.api;



import com.quad.track.model.BeatPointPOJO;
import com.quad.track.model.CommonPojo;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Armaan on 01-11-2017.
 */

public interface WebApi {

    //method for workaround
/*
    @Multipart
    @POST("{path}")
    Call<CommonPojo> makeRequestEmpRegisterMultipart
    (

            @Part("owner_id") RequestBody ownerid,
            @Part("firstname") RequestBody firstname,
            @Part("lastname") RequestBody lastname,
            @Part("fathername") RequestBody fathername,
            @Part("mothername") RequestBody mothername,
            @Part("age") RequestBody age,
            @Part("gender") RequestBody gender,
            @Part("joining_date") RequestBody joining_date,
            @Part("mobile") RequestBody mobile,
            @Part("native_address") RequestBody native_address,
            @Part("profilePic") RequestBody profilePic,
            @Part("idtype") RequestBody idtype,
            @Part("idnumber") RequestBody idnumber,
            @Part("idproof") RequestBody idproof,
            @Part("native_state") RequestBody native_state,
            @Part("native_district") RequestBody native_district,
            @Part("native_policestation") RequestBody native_policestation,
            @Part("living_status") RequestBody living_status,
            @Part("residential_address") RequestBody residential_address,
            //@Part("nearest_policestation") RequestBody nearest_policestation,
            @Part("reference_address1") RequestBody reference_address1,
            @Part("reference_address2") RequestBody reference_address2,
            @Part("occupation") RequestBody occupation,
            @Part("vehicle_type") RequestBody vehicle_type,
            @Part("vehicle_number") RequestBody vehicle_number,
            //@Part("vehicle_rc") RequestBody vehicle_rc,
            @Part("licence_number") RequestBody licence_number,
            //  @Part("driving_licence") RequestBody driving_licence,
            // @Part("scan1") RequestBody scan1,
            //@Part("scan2") RequestBody scan2,
            @Part("relieving_status") RequestBody relieving_status,
            @Part("phoneVerified") RequestBody phoneVerified,
            @Part List<MultipartBody.Part> myFiles,
            @Path(value = "path", encoded = true) String urlEndPoint

    );
*/





    @GET()
    Call<CommonPojo> getBeatPointList(@Url String urlEndPoint,
                            @QueryMap Map<String, String> params);

}
