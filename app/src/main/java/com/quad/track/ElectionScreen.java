package com.quad.track;
  
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.quad.track.api.RequestEndPoints;
import com.quad.track.api.RetrofitClient;
import com.quad.track.model.BeatPointPOJO;
import com.quad.track.model.CommonPojo;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ElectionScreen extends Activity {

	ImageButton takePictureButton;
	ImageButton updateButton;
	TextView pcName;
	TextView pcNumber;
	TextView pcPhone;
	AlertDialog.Builder alert;
	AlertDialog dialog;
	ImageButton beatPointListButton;
	ImageButton qrButton;
	ImageButton uploadBeatPoint;
	ImageButton newQRUpload;
	Button copyButton;
	ImageButton addBeatButton;
	static ElectionScreen instance;

	ProgressDialog progressDialog;
	ArrayList<BeatPointPOJO> allBeapointList;
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.electionscreen);
		instance=this;

		addBeatButton=(ImageButton)findViewById(R.id.totalBeatPointList);

		final ImageButton trackBtn = (ImageButton) findViewById(R.id.trackbutton);
		updateButton = (ImageButton) findViewById(R.id.updatebutton);
		SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		boolean onbeat=data.getBoolean("onbeat",false);

		if(onbeat)
		{
			updateButton.setImageResource(R.drawable.stop);
		}else
		{
			updateButton.setImageResource(R.drawable.start);
		}


/*		pcName = (TextView) findViewById(R.id.pcname);
		pcNumber = (TextView) findViewById(R.id.pcnumber);
		pcPhone = (TextView) findViewById(R.id.pcphone);*/
		qrButton=(ImageButton)findViewById(R.id.qrscan);
		copyButton=(Button)findViewById(R.id.copy);

		beatPointListButton = (ImageButton) findViewById(R.id.beatpoints);

		alert = new AlertDialog.Builder(this);

		uploadBeatPoint = (ImageButton) findViewById(R.id.uploadbeat);
		uploadBeatPoint.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ElectionScreen.this, Troubleshoot.class);

			/*	ProgressDialog pd = new ProgressDialog(ElectionScreen.this);
				//intent.putExtra("pd",new ArrayList());
				pd.setMessage("loading");
				pd.show();*/

				startActivity(intent);
				/*try{
					Thread.sleep(3000);
					pd.dismiss();
				}catch(Exception e)
				{
					e.printStackTrace();
				}*/

				//finish();
			}
		});

	/*	newQRUpload= (ImageButton) findViewById(R.id.newqrupload); //help
		newQRUpload.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				*//*Intent intent = new Intent(ElectionScreen.this, NewQRScanActivity.class);
				startActivity(intent);
				finish();*//*
				*//*Intent intent = new Intent(ElectionScreen.this, PrintQRMainActivity.class);
				startActivity(intent);*//*
				//finish();
			}
		});*/

		//final TextView timeSetting=(TextView)findViewById(R.id.timesetting);
		//timeSetting.setText();

		//Toast.makeText()


		trackBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ElectionScreen.this, TestgpsActivity.class);
				startActivity(intent);
				finish();

			}
		});

		takePictureButton = (ImageButton) findViewById(R.id.takepicture);

		takePictureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(ElectionScreen.this, StartPage.class);
				startActivity(intent);
				//finish();
			}
		});

		beatPointListButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ElectionScreen.this, BeatPointListActivity.class);
				startActivity(intent);
				//finish();


			}
		});


		addBeatButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				progressDialog=new ProgressDialog(ElectionScreen.this);
				progressDialog.setCanceledOnTouchOutside(false);
				progressDialog.show();
				SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				String deviceid = pref.getString("deviceid", null);
				Map<String,String> map=new HashMap<String, String>();
				map.put("deviceid",deviceid);


				Call<CommonPojo> call = RetrofitClient.getRestClient().getBeatPointList(RequestEndPoints.allbeats,map);
				call.enqueue(new Callback<CommonPojo>() {

				@Override
				public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
					try {

						if (response.isSuccessful()) {
							if (response.body().getMessage() != null && !response.body().getMessage().equalsIgnoreCase("Successful"))
							{
								showToast(response.body().getMessage());
							}
							if(response.body().getStatus())
							{
								// webResponseCallback.onResponse(response.body());
								allBeapointList=new ArrayList(response.body().getBeatPointPOJOList());
								Intent intent = new Intent(ElectionScreen.this, TotalBeatPointListActivity.class);
								intent.putExtra("beatPointsList",allBeapointList);
								startActivity(intent);
								//progressDialog.dismiss();
							}
							else
							{
								allBeapointList=new ArrayList<BeatPointPOJO>();
							}

						} else {
							JSONObject jObjError = new JSONObject(response.errorBody().string());
							showToast(jObjError.getString("message"));

							allBeapointList=new ArrayList<BeatPointPOJO>();
						}

						progressDialog.dismiss();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				@Override
				public void onFailure(Call<CommonPojo> call, Throwable t) {
					try {
						progressDialog.dismiss();
					   // webResponseCallback.failure();
						t.printStackTrace();
						if (t instanceof SocketTimeoutException || t instanceof ConnectException)
							showToast("Error downloading beat points");
					} catch (Exception e) {

					}
				}
			});


			}
		});
/*		updateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ElectionScreen.this, PrintQRMainActivity.class);
				startActivity(intent);
				//finish();

			}
		});*/

		qrButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ElectionScreen.this, QRMainActivity.class);
				startActivity(intent);
			}
		});


		updateButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				boolean onbeat=data.getBoolean("onbeat",false);
				if(onbeat)
				{
					alert.setTitle("Your Track is In Progress");
					LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
					final View layout = inflater.inflate(R.layout.stopbeatlayout, (ViewGroup) findViewById(R.id.stopbeatlayoutroot));

					pcName = (TextView) layout.findViewById(R.id.pcname);
					pcNumber = (TextView) layout.findViewById(R.id.pcnumber);
					pcPhone = (TextView) layout.findViewById(R.id.pcphone);
					pcName.setText(data.getString("pcname", ""));
					pcNumber.setText(data.getString("pcnumber", ""));
					pcPhone.setText(data.getString("pcphone", ""));

					alert.setView(layout);
					alert.setCancelable(false);
					alert.setPositiveButton("Stop", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {

							SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
							SharedPreferences.Editor editor = data.edit();
							editor.putBoolean("onbeat",false);
							editor.putBoolean("stopservice",true);
							editor.commit();
							boolean stop     = data.getBoolean("stopservice", false);



							stopService(new Intent(ElectionScreen.this,GPSService.class)); //
							stopService(new Intent(ElectionScreen.this,EmpTrackingService.class)); //   pfence



							updateButton.setImageResource(R.drawable.start);

							dialog.dismiss();

						}

					});
					alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.dismiss();
							//finish();

						}
					});

					dialog = alert.create(); // to prevent closing of dialog we need to create dialog from builder and overrride possitive button click after show() method.
					dialog.show();


				}else
				{
					alert.setTitle("Enter Your Details");
					LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
					final View layout = inflater.inflate(R.layout.pcdetailsdialog, (ViewGroup) findViewById(R.id.pcdialogroot));

					TextView pcnameTextView = (TextView) layout.findViewById(R.id.pcnametext);
					TextView pcnumberTextView = (TextView) layout.findViewById(R.id.pcnumbertext);
					TextView pcphoneTextView = (TextView) layout.findViewById(R.id.pcphonetext);

					//SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

					pcnameTextView.setText(data.getString("pcname", ""));
					pcnumberTextView.setText(data.getString("pcnumber", ""));
					pcphoneTextView.setText(data.getString("pcphone", ""));


					alert.setView(layout);
					alert.setCancelable(false);
					alert.setPositiveButton("Start", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {


						}

					});
					alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.dismiss();
							//finish();

						}
					});

					dialog = alert.create(); // to prevent closing of dialog we need to create dialog from builder and overrride possitive button click after show() method.
					dialog.show();

					dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {


							if(progressDialog!=null)
							{
								progressDialog.show();
							}

							LocationManager locationManager=(LocationManager)getSystemService(LOCATION_SERVICE);

							boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
							if (!isGPSEnabled) {
								showSettingsAlert();

							}else
							{
								Boolean wantToCloseDialog = false;
								//Do stuff, possibly set wantToCloseDialog to true then...

								TextView pcnameTextView = (TextView) layout.findViewById(R.id.pcnametext);
								TextView pcnumberTextView = (TextView) layout.findViewById(R.id.pcnumbertext);
								TextView pcphoneTextView = (TextView) layout.findViewById(R.id.pcphonetext);



								if (pcnameTextView == null || pcnumberTextView == null || pcphoneTextView == null) {
									Toast.makeText(getApplicationContext(), "Please Enter All Details", Toast.LENGTH_LONG).show();

								} else {
									//String s=pcphoneTextView.getText()+"";

									if(( !(pcphoneTextView.getText()+"").matches("[0-9]+") ||  (pcphoneTextView.getText()+"").length()!=10 ))
									{
										Toast.makeText(getApplicationContext(), "Please Enter Valid phone Number", Toast.LENGTH_LONG).show();
										return;
									}
									ConnectionDetector connectionDetector = new ConnectionDetector(ElectionScreen.this);
									if(connectionDetector.isConnectingToInternet()) {

										String pcname = pcnameTextView.getText().toString();
										String pcnumber = pcnumberTextView.getText().toString();
										String pcphone = pcphoneTextView.getText().toString();
										SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
										//String deviceId=data.getString("deviceid","0");
										SharedPreferences.Editor editor = data.edit();
										editor.putString("pcname", pcname);
										editor.putString("pcnumber", pcnumber);
										editor.putString("pcphone", pcphone);
										//editor.putBoolean("onbeat",true);

										editor.commit();

								/*		pcName.setText(data.getString("pcname", ""));
										pcNumber.setText(data.getString("pcnumber", ""));
										pcPhone.setText(data.getString("pcphone", ""));
*/
										//Intent fenceService = new Intent(ElectionScreen.this,EmpTrackingService.class);
										//startService(fenceService);




										// STOP AND START SERCIES..

										stopService(new Intent(ElectionScreen.this,GPSService.class)); //
										stopService(new Intent(ElectionScreen.this,EmpTrackingService.class)); //   pfence

										//data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
										String deviceid = data.getString("deviceid", null);
										String ipid = data.getString("ipid", IPInfo.IPADDRESS);
										long distance = data.getLong("distance",30);
										long time = data.getLong("time",0);
										long manualPollUpdatesTime = data.getLong("manualtimer",300000);


										///boolean disableDevice=data.getString("disabled")


										Intent gpsService = new Intent(ElectionScreen.this,GPSService.class);
										//PFENCE intent
										Intent fenceService = new Intent(ElectionScreen.this,EmpTrackingService.class);
										Bundle b = new Bundle();
										b.putString("deviceid", deviceid);
										b.putString("ipid", ipid);
										b.putLong("distance", distance);
										b.putLong("time", time);
										b.putLong("manualtimer", manualPollUpdatesTime);
										//adding this to forcedownload the beatpoints


										Bundle f = new Bundle();
										f.putBoolean("forceDownload",true);

										gpsService.putExtras(b);
										startService(gpsService);// main gps service
										//start PFence ServiceToo
										fenceService.putExtras(f);
										startService(fenceService);    // added at pfence integration


									/*	Toast.makeText(getApplicationContext(), "Your Track has Started!", Toast.LENGTH_LONG).show();
										//finish();
										editor = data.edit();
										editor.putBoolean("onbeat",true);
										editor.commit();
*/






										wantToCloseDialog = true;

										// END OF STOP AND START SERVICES

									}else
									{
										alertboxToLogin("Internet Connectivity Failed", "Make sure your device has Internet Connection");
										wantToCloseDialog = false;
									}




									//uploadBeatVisistsDownloadBeatpoints();




								}


								if (wantToCloseDialog) {

									dialog.dismiss();
								}
								//else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.



							} // end of GPS CHECK IF...




						}
					});


					//alert.show();

				} //end of else


			}// end of onClick
		});


		copyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				copyDatabase(ElectionScreen.this, "cravaka.db");

				//finish();
			}
		});



	}




	protected void showToast(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}



	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS Settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Go to settings menu?");
		alertDialog.setCancelable(false);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);

			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Showing Alert Message
		AlertDialog alertDialog1=alertDialog.create();
		alertDialog1.show();
	}
public void updateStartButton() {

	SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
	SharedPreferences.Editor editor = data.edit();
	editor = data.edit();
	editor.putBoolean("onbeat", true);
	editor.commit();

	updateButton.setImageResource(R.drawable.stop);
	if (progressDialog != null) {

		progressDialog.dismiss();

		Toast.makeText(this, "Your beat has started!", Toast.LENGTH_LONG).show();

	}
}

	public void updateStopButton()
	{
		if(progressDialog!=null)
		{
			progressDialog.dismiss();
		}


		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setTitle("Error");

		// set dialog message
		alertDialogBuilder
				.setMessage("There is a problem downloading Beatpoints! Check if BeatAbstract is available.")
				.setCancelable(false)

				.setNegativeButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

	/*	//There is error download beatpoints from server, so clear all the beat abstract.
		DBHelper db=new DBHelper(getApplicationContext());
		try{
			db.allDelete();
		}catch(Exception e){
			e.printStackTrace();
		}
*/

	}


	public void alertboxToLogin(String title, String msg) {
		if (msg != null) {
			new AlertDialog.Builder(this)
					.setMessage(msg)
					.setTitle(title)
					.setCancelable(true)
					.setNeutralButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){

								}
							}).show();
		}
	}

	public static void copyDatabase(Context c, String DATABASE_NAME) {
		String databasePath = c.getDatabasePath(DATABASE_NAME).getPath();
		//File f = new File(databasePath);
		File f = new File("/data/user/0/com.quad.track/databases/cravaka.db");

		OutputStream myOutput = null;
		InputStream myInput = null;
		Log.d("testing", " testing db path " + databasePath);
		Log.d("testing", " testing db exist " + f.exists());

		if (f.exists()) {
			try {

				File directory = new File("/mnt/sdcard/dinesh");
				if (!directory.exists())
					directory.mkdir();

				myOutput = new FileOutputStream(directory.getAbsolutePath()
						+ "/" + DATABASE_NAME);
				myInput = new FileInputStream("/data/user/0/com.quad.track/databases/"+DATABASE_NAME);

				byte[] buffer = new byte[1024];
				int length;
				while ((length = myInput.read(buffer)) > 0) {
					myOutput.write(buffer, 0, length);
				}

				myOutput.flush();
			} catch (Exception e) {
			} finally {
				try {
					if (myOutput != null) {
						myOutput.close();
						myOutput = null;
					}
					if (myInput != null) {
						myInput.close();
						myInput = null;
					}
				} catch (Exception e) {
				}
			}
		}
	}


	/*public static void copyDatabase(Context c, String DATABASE_NAME) {
		String databasePath = c.getDatabasePath(DATABASE_NAME).getPath();
		File f = new File(databasePath);
		OutputStream myOutput = null;
		InputStream myInput = null;
		Log.d("testing", " testing db path " + databasePath);
		Log.d("testing", " testing db exist " + f.exists());

		if (f.exists()) {
			try {

				File directory = new File("/storage/7A07-1F02");
				if (!directory.exists())
					directory.mkdir();

				myOutput = new FileOutputStream(directory.getAbsolutePath()
						+ "/" + DATABASE_NAME);
				myInput = new FileInputStream(databasePath);

				byte[] buffer = new byte[1024];
				int length;
				while ((length = myInput.read(buffer)) > 0) {
					myOutput.write(buffer, 0, length);
				}

				myOutput.flush();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (myOutput != null) {
						myOutput.close();
						myOutput = null;
					}
					if (myInput != null) {
						myInput.close();
						myInput = null;
					}
				} catch (Exception e) {
				}
			}
		}
	}*/


	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		String deviceId = data.getString("deviceid", "0");

		if (deviceId == null || "0".equalsIgnoreCase(deviceId)) {
			takePictureButton.setEnabled(false);
			updateButton.setEnabled(false);
			beatPointListButton.setEnabled(false);
			qrButton.setEnabled(false);


		} else {
			takePictureButton.setEnabled(true);
			updateButton.setEnabled(true);
			beatPointListButton.setEnabled(true);
			qrButton.setEnabled(true);
			String pcno = data.getString("pcnumber", "0");
			if (pcno == null || "0".equalsIgnoreCase(pcno)) {

			} else {
			/*	pcName.setText(data.getString("pcname", ""));
				pcNumber.setText(data.getString("pcnumber", ""));
				pcPhone.setText(data.getString("pcphone", ""));
*/

			}

		}

	}

	private static final int START_GPS = 1;
	private static final int UPLOAD_BEATPOINT = 2;
	private static final int UPLOAD_QR = 3;



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	/*	boolean enable=true;
		SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		//SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);


		String deviceid = pref.getString("deviceid", null);
		if (deviceid == null) {
			System.out.println(" Empid is not in the shared pref");
			enable=false;
		}


		MenuItem uploadBPBR = menu.add(0, UPLOAD_BEATPOINT, 1, "New Point Upload");
		uploadBPBR.setEnabled(enable);
		uploadBPBR.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//Toast.makeText(ElectionScreen.this,"Clicked",Toast.LENGTH_LONG).show();
				Intent intent = new Intent(ElectionScreen.this, AddBeatPoint.class);
				startActivity(intent);


				return true;
			}
		});

		MenuItem uploadQR = menu.add(0, UPLOAD_QR, 1, "New QR Upload ");
		uploadQR.setEnabled(enable);
		uploadQR.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//Toast.makeText(ElectionScreen.this,"Clicked",Toast.LENGTH_LONG).show();
				Intent intent = new Intent(ElectionScreen.this, NewQRScanActivity.class);
				startActivity(intent);
				return true;
			}
		});

		MenuItem settings = menu.add(0, START_GPS, 1, "Settings");
		settings.setEnabled(true);
		settings.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//Toast.makeText(ElectionScreen.this,"Clicked",Toast.LENGTH_LONG).show();
				Intent intent = new Intent(ElectionScreen.this, TestgpsActivity.class);
				startActivity(intent);
				finish();
				return true;
			}
		});
		MenuItem updateApp = menu.add(0, 2, 2, "Update App");
		updateApp.setEnabled(true);
		*//*updateApp.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//Toast.makeText(ElectionScreen.this,"Clicked",Toast.LENGTH_LONG).show();
				Intent intent = new Intent(ElectionScreen.this, MainActivity.class);
				startActivity(intent);
				finish();
				return true;
			}
		});*//*

*/
		return super.onCreateOptionsMenu(menu);
	}


	/*private class DownloadBeatPointsTask extends AsyncTask<String, Void, String> {
		String output = null;
		String data = "";
		private final HttpClient Client = new DefaultHttpClient();
		private String Content;
		private String Error = null;
		private boolean noRecords = false;
		private String empid;
		ArrayList<String> dealersList = null;
		JSONArray jArray = new JSONArray();
		boolean download = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//SharedPreferences pref = getSharedPreferences("Setting", MODE_PRIVATE);
			SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
			//SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);


			empid = pref.getString("deviceid", null);
			if (empid == null) {
				System.out.println(" Empid is not in the shared pref");
			}


	*//*		SharedPreferences data1=getSharedPreferences("datastore",Context.MODE_WORLD_READABLE);
			if(data1.getBoolean("beatPointUpdate",false))
			{

				download=true;
			}*//*
		}

		@Override
		protected String doInBackground(String... urls) {
			output = upload();
			if (output == null) // this is when there is error
			{
				output = "FAILURE";
			} else if (output.equalsIgnoreCase("EMPTY")) // this is the case for first timers...
			{
				output = download();

			} else {
				SharedPreferences data1 = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
				if (data1.getBoolean("beatPointUpdate", false)) {
					//asssuming download will be success
					SharedPreferences.Editor edit = data1.edit(); // ok i have taken care of updating beat points..now setting back flag to false...
					edit.putBoolean("beatPointUpdate", false);
					edit.commit();

					output = download();


					if (!output.equalsIgnoreCase("SUCCESS")) {
						//SharedPreferences data1 = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
						//SharedPreferences.Editor edit = data1.edit(); // ok i have taken care of updating beat points..now setting back flag to false...
						edit.putBoolean("beatPointUpdate", true);
						edit.commit();


						//String url=""


						*//*	String url="http://www.epatrol.in:9092/vehicleservice/beatpointdownloadack?empid="+empid;
							String result=sendAckForBeatPointDownload(url);
	*//*


					}


				}


			}
			return output;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (output.equalsIgnoreCase("SUCCESS")) {
				Toast.makeText(getApplicationContext(), "SUCS: BEAT POINTS!!", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), "PWDBP: BEAT POINTS!", Toast.LENGTH_LONG).show();
				return;
			}
		}


	}


	public String download()
	{
		BufferedReader reader=null;
		DBHelper db=new DBHelper(getApplicationContext());
		//String url="http://192.168.1.139:2018/vehicleservice/getlocations?empid="+empid; //10.0.2.2
		//String url="http://137.59.201.89:2018/vehicleservice/getlocations?empid="+empid; //10.0.2.2
		String url="http://www.epatrol.in:9092/vehicleservice/getlocations?empid="+empid; //10.0.2.2
		String readJSON = getJSON(url);
		try{
			if(readJSON!=null && readJSON.trim().equalsIgnoreCase("NORECORDS"))
			{
				noRecords=true;
				return "FAILURE";
			}
			System.out.println(readJSON);
			jArray= new JSONArray(readJSON);
		} catch(Exception e){
			e.printStackTrace();
			return "FAILURE";
		}
		JSONObject jObj = null;
		String empid=null,dealerId=null,name=null,lat=null,lon=null;
		boolean disabled=false;
		String inTime="",outTime="",beatPointType="";
		dealersList=new ArrayList<String>();
		try{
			db.allDelete();
		}catch(Exception e){
			e.printStackTrace();
		}
		for(int j=0;j<jArray.length();j++){
			try {
				jObj= jArray.getJSONObject(j);
				empid = jObj.getString("empid");
				dealerId = jObj.getString("DealerId");
				name = jObj.getString("DealerName");
				lat = jObj.getString("Lat");
				lon = jObj.getString("Lon");
				//adding new parameters to database
				disabled=jObj.getBoolean("disabled");
				inTime=jObj.getString("inTime"); // no chance of null as we are sending default values from server.
				outTime=jObj.getString("outTime"); // no chance of null as we are sending default values from server.
				beatPointType=jObj.getString("beatPointType");  // getting beat point type for image/flag display...
				dealersList.add(dealerId+" "+name+""+lat+""+lon);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//db.insertDealer(empid, dealerId, name, lat, lon);



			//db.insertDealer(empid, dealerId, name, lat, lon,disabled,inTime,outTime);

			db.insertDealer(empid, dealerId, name, lat, lon,disabled,inTime,outTime,beatPointType);

		}
		return "SUCCESS";
	}

	public String upload()
	{
		String responseString = null;
		try
		{

			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			//HttpPost httppost = new HttpPost("http://192.168.1.139:2018/vehicleservice/uploaddealervisitdetails");
			//HttpPost httppost = new HttpPost("http://137.59.201.89:2018/vehicleservice/uploaddealervisitdetails");
			HttpPost httppost = new HttpPost("http://www.epatrol.in:9092/vehicleservice/uploaddealervisitdetails");

			JSONArray jArray=new JSONArray();
			JSONObject obj;
			DBHelper db=new DBHelper(getApplicationContext());

			// added by dinesh
			if(db.getAlarm()==null || db.getAlarm().size()==0) // which means there are no beat points.. so send upload as success
			{
				responseString="EMPTY"; // we are sending success so that download can happen..
				return "EMPTY";
			}



			ArrayList<DealerBean> uploadArrayList =db.getAllVisitDetails();
			if(uploadArrayList!=null)
			{
				for(DealerBean alarm:uploadArrayList )
				{
					obj=new JSONObject();
					try{
						SimpleDateFormat datePattern = new SimpleDateFormat("yyyy-MM-dd");
						obj.put("date",datePattern.format(alarm.getDate()));
					}catch(Exception e)
					{
						e.printStackTrace();
						continue;
					}
					obj.put("dealerId", alarm.getDealerId());
					obj.put("empid",alarm.getEmpid());
					SimpleDateFormat timePattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String inTime = null, outTime = null;
					try{
						if(alarm.getInTime()!=null){
							inTime = timePattern.format(alarm.getInTime());
							obj.put("inTime",inTime);
						}if(alarm.getOutTime()!=null){
							outTime=timePattern.format(alarm.getOutTime());
							obj.put("outTime",outTime);
						}if(inTime!=null && outTime!=null){
							obj.put("timespent",alarm.getTimeSpent());
						}
					}catch(Exception e)
					{
						e.printStackTrace();
						continue;
					}
					jArray.put(obj);
				}
			}
			if(jArray.length()!=0){
				StringEntity se = new StringEntity(jArray.toString());
				httppost.setEntity(se);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity resEntity = response.getEntity();
				responseString = EntityUtils.toString(resEntity, "UTF-8");
				if(responseString!=null && responseString.equalsIgnoreCase("SUCCESS"))
				{
					db.deleteVisitDetails();
				}
				if (resEntity != null) {
					System.out.println(response.getStatusLine());
				}
				if(responseString==null){
					responseString="NO MATCHES FOUND";
				}
				if (resEntity != null) {
					resEntity.consumeContent();
				}
				httpclient.getConnectionManager().shutdown();
			}else
			{
				responseString ="SUCCESS";
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return responseString;
	}

	public String getJSON(String address){
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(address);
		try{
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if(statusCode == 200){
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while((line = reader.readLine()) != null){
					builder.append(line);
				}
			} else {
				Log.e(this.toString(),"Failed at JSON object");
			}
		}catch(ClientProtocolException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		return builder.toString();
	}
*/

}

