package com.quad.track;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.Settings;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class StartAtBootServiceReceiver extends BroadcastReceiver {
	
		@Override
		public void onReceive(Context context, Intent intent) 
		{


			/* POWER ON AND POWER OFF TIMES */

		/*SharedPreferences prefs = context.getSharedPreferences("datastore",
                    Context.MODE_WORLD_READABLE);

		DateFormat dateFomat=new SimpleDateFormat("MMM, dd yyyy hh:mm:ss a");
		String date=dateFomat.format(new Date());

        SharedPreferences.Editor edit=prefs.edit();
        edit.putString("boottime",date);

		edit.commit();*/


			/*END OF POWER ON AND OFF */

            //DEVICE POWER ON
            DatabaseHandler databaseHandler = new DatabaseHandler(context);
            System.out.println(" String up .....");
            try {
                Date date = new Date();
                DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
                String currentDateTimeString = utilDateFormatter.format(date);
                databaseHandler.addData("ON", currentDateTimeString, "POWER");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //END OF DEVICE POWER ON

			if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				
				 try {
					  final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
		        	  final Class conmanClass = Class.forName(conman.getClass().getName());
		        	  final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
		        	  iConnectivityManagerField.setAccessible(true);
		        	  final Object iConnectivityManager = iConnectivityManagerField.get(conman);
		        	  final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
		        	  final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
		        	  setMobileDataEnabledMethod.setAccessible(true);
		              setMobileDataEnabledMethod.invoke(iConnectivityManager, true);
				 }
			      catch(Exception e) {
				    
				 	
			      }
				 
				 
				 
				 try {
					String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
					 
					 if(!provider.contains("gps")) {
						 
						 Intent gpsIntent = new Intent("android.location.GPS_ENABLED_CHANGE");
					     intent.putExtra("enabled", true);
					     context.sendBroadcast(gpsIntent);
						 final Intent poke = new Intent();
					     poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider"); 
					     poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
					     poke.setData(Uri.parse("3")); 
					     context.sendBroadcast(poke);
					
					 }
				} catch (Exception e) {
					
				}
				 
				 
				 
			
			 Intent i = new Intent();
			 i.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
		     context.startService(i);
		}
	}
}