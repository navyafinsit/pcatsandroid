package com.quad.track;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EmpTrackingService extends Service {
	SharedPreferences pref;
	private static final long MINIMUM_DISTANCECHANGE_FOR_UPDATE = 20; 
	private static final long MINIMUM_TIME_BETWEEN_UPDATE = 15000; //15000
	ArrayList<DealerBean> arrayList;
	private static int a;
	private LocationManager locationManager;
	ArrayList<DealerBean> activeAlarmList=new ArrayList<DealerBean>();
	Date today;

	//ADDED BY DINESH FOR BROADCASTING MESSAGE
	static final public String EMPTRACKING_RESULT = "com.quad.pfence.emptrackingservice.REQUEST_PROCESSED";
	static final public String EMPTRACKING_MESSAGE = "com.quad.pfence.emptrackingservice.FENCE_MESSAGE";
	static final public String EMPTRACKING_BEAT_POINT_NAME = "com.quad.pfence.emptrackingservice.EMPTRACKING_BEAT_POINT_NAME";
	LocalBroadcastManager broadcaster;

	ArrayList<Messenger> mClients = new ArrayList<Messenger>(); // Keeps track of all current registered clients.
	int mValue = 0; // Holds last value set by a client.
	static final int MSG_REGISTER_CLIENT = 1;
	static final int MSG_UNREGISTER_CLIENT = 2;
	static final int MSG_SET_INT_VALUE = 3;
	static final int MSG_SET_STRING_VALUE = 4;
	final Messenger mMessenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.

	@Override
	public void onCreate() {
		pref = getSharedPreferences("Setting", MODE_PRIVATE);
		int getGps = pref.getInt("gps", 0);
		arrayList = new ArrayList<DealerBean>();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MINIMUM_TIME_BETWEEN_UPDATE, MINIMUM_DISTANCECHANGE_FOR_UPDATE,new MyLocationListener());

		//broadcase
		 broadcaster = LocalBroadcastManager.getInstance(this);

		SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

		// call upload to download beatpoints from server..
		/*UploadTask task = new UploadTask();
		task.execute();*/


	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {


		/*UploadTask task = new UploadTask(true);
		task.execute();
*/
		if(intent !=null )
		{
			Bundle b=intent.getExtras();
			boolean forceDownload=b.getBoolean("forceDownload",false);
			if(forceDownload )
			{
				UploadTask task = new UploadTask(true);
				task.execute();
			}else
			{
				UploadTask task = new UploadTask();
				task.execute();
			}
		}



		return super.onStartCommand(intent, flags, startId);
	}


	class IncomingHandler extends Handler { // Handler of incoming messages from clients.
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_REGISTER_CLIENT:
					mClients.add(msg.replyTo);
					break;
				case MSG_UNREGISTER_CLIENT:
					mClients.remove(msg.replyTo);
					break;

				default:
					super.handleMessage(msg);
			}
		}
	}


	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	public class MyLocationListener implements LocationListener {
		private static final int notificationId = 1;	
		public void onLocationChanged(Location location) {
			//Toast.makeText(getApplicationContext(), "Location changed for Fence", Toast.LENGTH_SHORT).show();
			if(location.getAccuracy()>30.0){
				return;

			}
			DBHelper dbHelper = new DBHelper(EmpTrackingService.this);
			double currentLat = location.getLatitude();
			double currentLon = location.getLongitude();
		/*	if(today!= null && today.before(new Date()))
			{
			}else
			{
				today=new Date();*/
				arrayList = dbHelper.getAlarm();
			/*}		*/
			int getRange =50;
			for (DealerBean alarm : arrayList) {
				double trgLat = Double.parseDouble(alarm.getLatitude());
				double trgLon = Double.parseDouble(alarm.getLongitude());
				String titleString = alarm.getDealerId();
				String notificationString = alarm.getDealerName();
				a=0;
				if (a== 0) {
					long dis = (long) EmpTrackingService.distFrom(trgLat,trgLon, currentLat, currentLon);
					//Toast.makeText(getApplicationContext(),"dis" + dis, Toast.LENGTH_SHORT).show();
					if (dis <= getRange) {
					//Toast.makeText(getApplicationContext(),"dis <= getRange " + dis+ " "+getRange, Toast.LENGTH_SHORT).show();
						if(!alarm.isInside())
						{
							//added by dinesh to check for disabled and time..
							if(alarm.isDisabled() || !calculateTime(alarm.getDefinedInTime(),alarm.getDefinedOutTime()) || "QR".equalsIgnoreCase(alarm.getBeatPointType()))
							{
								continue; // skip this beat point go to next alarm
							}



							//alarm.setInside(true);
							String datePattern = "yyyy-MM-dd";
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
							String date = simpleDateFormat.format(new Date());
							String timePattern = "yyyy-MM-dd HH:mm:ss";
							SimpleDateFormat simpleTimeFormat = new SimpleDateFormat(timePattern);
							String inTime = simpleTimeFormat.format(new Date());
							SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
							//String empid=pref.getString("empid","");
							String empid=pref.getString("deviceid","");
							long dbRowId=dbHelper.insertDealerVisitDetails(date, alarm.getDealerId(), empid, inTime, null, null);

							//UPDATE BEAT VISITED STATUS
							dbHelper.updateBeatVisitedStatus(alarm.getDealerId(),"Y");
							alarm.setInsertRowId(dbRowId);
							/*//broadcast message to UI
							Intent intent = new Intent(EMPTRACKING_RESULT);
							intent.putExtra(EMPTRACKING_MESSAGE, "ENTER");
							intent.putExtra(EMPTRACKING_BEAT_POINT_NAME,alarm.getDealerName());
							broadcaster.sendBroadcast(intent);
							//END OF BROADCASTING*/
							Toast.makeText(getApplicationContext(),"Inside Beat Location: " + titleString, Toast.LENGTH_SHORT).show();
						}
					} else if (dis > getRange ) {
						//Toast.makeText(getApplicationContext(),"dis > getRange " + dis+ " "+getRange, Toast.LENGTH_SHORT).show();
						if(alarm.isInside())
						{
							//Toast.makeText(getApplicationContext(),"Inside Beat Location: " + dis+ " "+getRange, Toast.LENGTH_SHORT).show();
							//alarm.setInside(false);
							if((dis-getRange) < 500)
							{									
								String timePattern = "yyyy-MM-dd HH:mm:ss";
								SimpleDateFormat simpleTimeFormat = new SimpleDateFormat(timePattern);
								String outTime = simpleTimeFormat.format(new Date());

							/*	//BROADCASE MESSAGE TO SERVER
								Intent intent = new Intent(EMPTRACKING_RESULT);
								intent.putExtra(EMPTRACKING_MESSAGE, "EXIT");
								intent.putExtra(EMPTRACKING_BEAT_POINT_NAME,alarm.getDealerName());
								broadcaster.sendBroadcast(intent);
								//END OF BROADCAST*/

								Toast.makeText(getApplicationContext(),"Exiting Beat Location: " + titleString, Toast.LENGTH_SHORT).show();
								long insertedRowId=alarm.getInsertRowId();								
								dbHelper.updateExitTime((int)insertedRowId, outTime, null);

								// call service to update online server.
								UploadTask task = new UploadTask();
								task.execute();
								// end of added code by dinesh...

							}else // put a time spent as  1 minute 1 second,  to identify that there is a battery lost / switched off..etc..
							{

							}
						}
					}

					SharedPreferences data1=getSharedPreferences("datastore",Context.MODE_WORLD_READABLE);
					if(data1.getBoolean("beatPointUpdate",false))
					{
						UploadTask task = new UploadTask();
						task.execute();
					}


				} else {
					Toast.makeText(getApplicationContext(),"...", Toast.LENGTH_SHORT).show();
				}
			}




		}


		public boolean calculateTime(String inTime,String outTime)
		{


			Calendar cal = Calendar.getInstance();

			Time currentTime = Time.valueOf(
					cal.get(Calendar.HOUR_OF_DAY) + ":" +
							cal.get(Calendar.MINUTE) + ":" +
							cal.get(Calendar.SECOND) );


			try{
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

				long ms1 = sdf.parse(inTime).getTime();
				Time t1 = new Time(ms1);
				long ms2 = sdf.parse(outTime).getTime();
				Time t2 = new Time(ms2);

				Calendar cal1 = Calendar.getInstance();
				cal1.setTime(t1);
				Calendar cal2 = Calendar.getInstance();
				cal2.setTime(t2);

				int x1=Calendar.PM;
				int y1=cal1.get(Calendar.AM_PM);
				int x2=Calendar.AM;
				int y2=cal2.get(Calendar.AM_PM);


				if(x1==y1 && x2==y2) // THIS IS FOR PM AM
				{
					//CALENDAR FOR 12 AM
					Calendar cal12AM = Calendar.getInstance();
					//cal12AM.add(Calendar.DATE,1);// ADDING 1 TO GET ZERO HOUR OF NEXT DAY....
					cal12AM.set(Calendar.HOUR_OF_DAY,23);
					cal12AM.set(Calendar.MINUTE,59);
					cal12AM.set(Calendar.SECOND,59);
					Time zeroTime1=Time.valueOf(	cal12AM.get(Calendar.HOUR_OF_DAY) + ":" +
							cal12AM.get(Calendar.MINUTE) + ":" +
							cal12AM.get(Calendar.SECOND) );

					Time zeroTime2=Time.valueOf("00:00:00");

					if(currentTime.after(t1) && currentTime.before(zeroTime1) ||  currentTime.after(zeroTime2) && currentTime.before(t2))
					{
						return true;
					}else
					{
						return false;
					}
				}



				if(currentTime.after(t1) && currentTime.before(t2)) // this if for AM AM , AM PM , PM PM
				{
					return true;
				}else
				{
					return false;
				}

			}catch(java.text.ParseException pe)
			{
				pe.printStackTrace();
			}



			return false;

		}

		public void onStatusChanged(String s, int i, Bundle b) {
		}
		public void onProviderDisabled(String s) {
		}
		public void onProviderEnabled(String s) {
		}
	}
	public static double distFrom(double lat1, double lng1, double lat2,
			double lng2) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
				* Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;
		int meterConversion = 1609;
		return (dist * meterConversion);
	}
	/*@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
*/

 /*
 This class is moved from AsyncUploadServcie
  */
	private class  UploadTask extends AsyncTask<String, Void, String> {
		String output=null;
		String data="";
		private final HttpClient Client = new DefaultHttpClient();
		private String Content;
		private String Error = null;
		private boolean noRecords=false;
		private String empid;
		ArrayList<String> dealersList = null;
		JSONArray jArray=new JSONArray();
		boolean download=false;
	    boolean forceDownload=false;
	 public UploadTask()
	 {

	 }

	 public UploadTask(boolean forceDownload)
	 {
		 this.forceDownload=forceDownload;
	 }

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//SharedPreferences pref = getSharedPreferences("Setting", MODE_PRIVATE);
			SharedPreferences pref = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
			//SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);


			empid = pref.getString("deviceid", null);
			if (empid == null) {
				System.out.println(" Empid is not in the shared pref");
			}


	/*		SharedPreferences data1=getSharedPreferences("datastore",Context.MODE_WORLD_READABLE);
			if(data1.getBoolean("beatPointUpdate",false))
			{

				download=true;
			}*/
		}
		@Override
		protected String doInBackground(String... urls) {
			output = upload();
			if(output==null) // this is when there is error
			{
				output="FAILURE";
			}else if(output.equalsIgnoreCase("EMPTY")) // this is the case for first timers...
			{
				output=download();

			}else if(forceDownload && output.equalsIgnoreCase("SUCCESS"))
			{
					forceDownload=false;
					output=download();
					//just a comment

			}
			else
			{
					SharedPreferences data1=getSharedPreferences("datastore",Context.MODE_WORLD_READABLE);
					if(data1.getBoolean("beatPointUpdate",false))
					{
						//asssuming download will be success
						SharedPreferences.Editor edit = data1.edit(); // ok i have taken care of updating beat points..now setting back flag to false...
						edit.putBoolean("beatPointUpdate", false);
						edit.commit();

						output=download();


						if(!output.equalsIgnoreCase("SUCCESS")) {
							//SharedPreferences data1 = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
							//SharedPreferences.Editor edit = data1.edit(); // ok i have taken care of updating beat points..now setting back flag to false...
							edit.putBoolean("beatPointUpdate",true);
							edit.commit();


							//String url=""


						/*	String url="http://www.epatrol.in:9092/vehicleservice/beatpointdownloadack?empid="+empid;
							String result=sendAckForBeatPointDownload(url);
	*/


						}
					}




			}



			return output;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if(output.equalsIgnoreCase("SUCCESS"))
			{
				Toast.makeText(getApplicationContext(), "SUCS: BEAT POINTS!!", Toast.LENGTH_LONG).show();
				// need to change here dinesh
				ElectionScreen activity = ElectionScreen.instance;

				if (activity != null) {
					// we are calling here activity's method
					activity.updateStartButton();
				}

			}else
			{
				Toast.makeText(getApplicationContext(), "PWDBP: BEAT POINTS!", Toast.LENGTH_LONG).show();

/*
				AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext())
						.setTitle("Title")
						.setMessage("Are you sure?")
						.create();

				alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
				alertDialog.show();
*/

				ElectionScreen activity = ElectionScreen.instance;

				if (activity != null) {
					// we are calling here activity's method
					activity.updateStopButton();
				}


				return;
			}
		}

		public String getJSON(String address){
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(address);
			try{
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if(statusCode == 200){
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(content));
					String line;
					while((line = reader.readLine()) != null){
						builder.append(line);
					}
				} else {
					Log.e(this.toString(),"Failed at JSON object");
				}
			}catch(ClientProtocolException e){
				e.printStackTrace();
			} catch (IOException e){
				e.printStackTrace();
			}
			return builder.toString();
		}


	 public String sendAckForBeatPointDownload(String address){
		 StringBuilder builder = new StringBuilder();
		 HttpClient client = new DefaultHttpClient();
		 HttpGet httpGet = new HttpGet(address);
		 try{
			 HttpResponse response = client.execute(httpGet);
			 StatusLine statusLine = response.getStatusLine();
			 int statusCode = statusLine.getStatusCode();
			 if(statusCode == 200){
				 HttpEntity entity = response.getEntity();
				 InputStream content = entity.getContent();
				 BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				 String line;
				 while((line = reader.readLine()) != null){
					 builder.append(line);
				 }
			 } else {
				 Log.e(this.toString(),"Failed at JSON object");
			 }
		 }catch(ClientProtocolException e){
			 e.printStackTrace();
		 } catch (IOException e){
			 e.printStackTrace();
		 }
		 return builder.toString();
	 }

		public String download()
		{
			BufferedReader reader=null;
			DBHelper db=new DBHelper(getApplicationContext());
			//String url="http://192.168.1.139:2018/vehicleservice/getlocations?empid="+empid; //10.0.2.2
			//String url="http://137.59.201.89:2018/vehicleservice/getlocations?empid="+empid; //10.0.2.2
			//String url="http://www.epatrol.in:9092/vehicleservice/getlocations?empid="+empid; //10.0.2.2

			SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

			String pcname=data.getString("pcname", "");
			String pcnumber=data.getString("pcnumber", "");
			String pcphone=data.getString("pcphone", "");


			String url=IPInfo.HTTP_IPADDRESS+"/vehicleservice/getlocations?empid="+empid+"&pcname="+pcname+"&pcnumber="+pcnumber+"&pcphone="+pcphone; //10.0.2.2

			//String urlStr = "http://www.epatrol.in:9092/vehicleservice/uploaddealervisitdetails?pcname="+pcname+"&pcnumber="+pcnumber+"&pcphone="+pcphone;

			url = url.toString().replace(" ", "%20");
			//URL url = new URL(urlStr);
			//HttpPost httppost = new HttpPost(urlStr);


			String readJSON = getJSON(url);
			try{
				if(readJSON!=null && readJSON.trim().equalsIgnoreCase("NORECORDS"))
				{
					noRecords=true;

					return "FAILURE";
				}
				System.out.println(readJSON);
				jArray= new JSONArray(readJSON);
			} catch(Exception e){
				e.printStackTrace();
				return "FAILURE";
			}
			JSONObject jObj = null;
			String empid=null,dealerId=null,name=null,lat=null,lon=null;
			boolean disabled=false;
			String inTime="",outTime="",beatPointType="",qrCode="";
			dealersList=new ArrayList<String>();
			try{
				db.allDelete();
			}catch(Exception e){
				e.printStackTrace();
			}
			for(int j=0;j<jArray.length();j++){
				try {
					jObj= jArray.getJSONObject(j);
					empid = jObj.getString("empid");
					dealerId = jObj.getString("DealerId");
					name = jObj.getString("DealerName");
					lat = jObj.getString("Lat");
					lon = jObj.getString("Lon");
					//adding new parameters to database
					disabled=jObj.getBoolean("disabled");
					inTime=jObj.getString("inTime"); // no chance of null as we are sending default values from server.
					outTime=jObj.getString("outTime"); // no chance of null as we are sending default values from server.
					beatPointType=jObj.getString("beatPointType");  // getting beat point type for image/flag display...
					if("QR".equalsIgnoreCase(beatPointType))
					{
						qrCode=jObj.getString("qrCode");
					}

					dealersList.add(dealerId+" "+name+""+lat+""+lon);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				//db.insertDealer(empid, dealerId, name, lat, lon);



				//db.insertDealer(empid, dealerId, name, lat, lon,disabled,inTime,outTime);

				db.insertDealer(empid, dealerId, name, lat, lon,disabled,inTime,outTime,beatPointType,qrCode);

			}
			return "SUCCESS";
		}

		public String upload()
		{
			String responseString = null;
			try
			{

				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				//HttpPost httppost = new HttpPost("http://192.168.1.139:2018/vehicleservice/uploaddealervisitdetails");
				//HttpPost httppost = new HttpPost("http://137.59.201.89:2018/vehicleservice/uploaddealervisitdetails");
				//HttpPost httppost = new HttpPost("http://www.epatrol.in:9092/vehicleservice/uploaddealervisitdetails");
				HttpPost httppost = new HttpPost(IPInfo.HTTP_IPADDRESS+"/vehicleservice/uploaddealervisitdetails");


			/*	SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

				String pcname=data.getString("pcname", "");
				String pcnumber=data.getString("pcnumber", "");
				String pcphone=data.getString("pcphone", "");


				String urlStr = "http://www.epatrol.in:9092/vehicleservice/uploaddealervisitdetails?pcname="+pcname+"&pcnumber="+pcnumber+"&pcphone="+pcphone;

				urlStr = urlStr.toString().replace(" ", "%20");
				URL url = new URL(urlStr);
				HttpPost httppost = new HttpPost(urlStr);*/

				JSONArray jArray=new JSONArray();
				JSONObject obj;
				DBHelper db=new DBHelper(getApplicationContext());

				// added by dinesh
				if(db.getAlarm()==null || db.getAlarm().size()==0) // which means there are no beat points.. so send upload as success
				{
					responseString="EMPTY"; // we are sending success so that download can happen..
					return "EMPTY";
				}



				ArrayList<DealerBean> uploadArrayList =db.getAllVisitDetails();
				if(uploadArrayList!=null)
				{
					for(DealerBean alarm:uploadArrayList )
					{
						obj=new JSONObject();
						try{
							SimpleDateFormat datePattern = new SimpleDateFormat("yyyy-MM-dd");
							obj.put("date",datePattern.format(alarm.getDate()));
						}catch(Exception e)
						{
							e.printStackTrace();
							continue;
						}
						obj.put("dealerId", alarm.getDealerId());
						obj.put("empid",alarm.getEmpid());

						obj.put("type",alarm.getBeatPointType());
						if("QR".equalsIgnoreCase(alarm.getBeatPointType()))
						{
							obj.put("qrtime",alarm.getInTime());
							obj.put("lat",alarm.getLatitude());
							obj.put("lng",alarm.getLongitude());
							obj.put("qrcode",alarm.getQrcode());

						}


						SimpleDateFormat timePattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String inTime = null, outTime = null;
						try{
							if(alarm.getInTime()!=null){
								inTime = timePattern.format(alarm.getInTime());
								obj.put("inTime",inTime);
							}if(alarm.getOutTime()!=null){
								outTime=timePattern.format(alarm.getOutTime());
								obj.put("outTime",outTime);
							}if(inTime!=null && outTime!=null){
								obj.put("timespent",alarm.getTimeSpent());
							}
						}catch(Exception e)
						{
							e.printStackTrace();
							continue;
						}
						jArray.put(obj);
					}
				}
				if(jArray.length()!=0){
					StringEntity se = new StringEntity(jArray.toString());
					httppost.setEntity(se);
					httppost.setHeader("Accept", "application/json");
					httppost.setHeader("Content-type", "application/json");
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity resEntity = response.getEntity();
					responseString = EntityUtils.toString(resEntity, "UTF-8");
					if(responseString!=null && responseString.equalsIgnoreCase("SUCCESS"))
					{
						db.deleteVisitDetails();
					}
					if (resEntity != null) {
						System.out.println(response.getStatusLine());
					}
					if(responseString==null){
						responseString="NO MATCHES FOUND";
					}
					if (resEntity != null) {
						resEntity.consumeContent();
					}
					httpclient.getConnectionManager().shutdown();
				}else
				{
					responseString ="SUCCESS";
				}
			}catch(Exception e)
			{
				e.printStackTrace();
				return null;
			}
			return responseString;
		}
	}

}
