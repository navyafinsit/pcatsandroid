package com.quad.track;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BeatPointListActivity extends Activity {

   //ArrayAdapter arrayAdapter;
    ArrayList<String> dealersList=new ArrayList<String>();
    BeatPointListAdapter beatPointAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.beatpointlist);

        //arrayAdapter=new ArrayAdapter<String>(this,R.layout.rowlayout,R.id.label, dealersList);

/*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.rowlayout, R.id.label, values);*/


        DBHelper dbHelper = new DBHelper(this);
       // SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);

      final  ArrayList<DealerBean> beatPoints=dbHelper.getAlarm();

        beatPointAdapter=new BeatPointListAdapter(this,beatPoints);


     /*   for(DealerBean db:beatPoints)
        {
            arrayAdapter.add(db.getDealerName()+ " "+ db.getDefinedInTime() + " " +db.getDefinedOutTime());
        }*/



        ListView lv =(ListView)findViewById(R.id.list);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.listheader, lv,false);
        lv.addHeaderView(header);
        lv.setAdapter(beatPointAdapter);
        lv.setDivider(new ColorDrawable(0x99FFFFFF));
        lv.setDividerHeight(2);

        dbHelper.close();


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  Toast.makeText(BeatPointListActivity.this,"Hello"+position,Toast.LENGTH_LONG).show();
                DealerBean db= beatPointAdapter.itemList.get(position-1);

                String definedInTime=db.getDefinedInTime();
                String definedOutTime=db.getDefinedOutTime();

                try {
                    String string1 = definedInTime;//"05:11:13";
                    Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(time1);

                    String string2 = definedOutTime;//"06:29:00";
                    Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTime(time2);
                    // calendar2.add(Calendar.DATE, 1);

                    String someRandomTime = new SimpleDateFormat("HH:mm:yy").format(Calendar.getInstance().getTime());
                    Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.setTime(d);
                    //calendar3.add(Calendar.DATE, 1);



                    if(calendar1.get(Calendar.AM_PM)==Calendar.PM && calendar2.get(Calendar.AM_PM)==Calendar.AM)
                    {
                        calendar2.add(Calendar.DATE, 1);
                        if(calendar3.get(Calendar.AM_PM)==Calendar.AM)
                        {
                            calendar3.add(Calendar.DATE, 1);
                        }

                    }

                    Date x = calendar3.getTime();

                    if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                        //checkes whether the current time is between 14:49:00 and 20:11:13.

                    }else
                    {
                        Toast.makeText(getApplicationContext(),"Sorry you are not in specified time!!",Toast.LENGTH_LONG).show();
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if("QR".equalsIgnoreCase(db.getBeatPointType()))
                {
                    Intent intent = new Intent(BeatPointListActivity.this, QRMainActivity.class);
                    intent.putExtra("dealerId",db.getDealerId());
                    startActivity(intent);
                }else
                {
                    Intent intent = new Intent(BeatPointListActivity.this, StartPage.class);
                    intent.putExtra("dealerId",db.getDealerId());
                    startActivity(intent);
                }


            }
        });




        ImageButton addBeatPointButton=(ImageButton)findViewById(R.id.addBeatPointBtn);
        addBeatPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUpMenu(v);
            }
        });

        ImageButton showMapPointBtn=(ImageButton)findViewById(R.id.showMapPointBtn);
        showMapPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showPopUpMenu(v);

               Intent i = new Intent(BeatPointListActivity.this, LocationActivity.class);
               i.putExtra("beatPoints",beatPoints);
                startActivity(i);
            }
        });




    }



    private void showPopUpMenu(final View imageView) {
        PopupMenu mMenu = new PopupMenu(BeatPointListActivity.this, imageView);
        mMenu.getMenuInflater().inflate(R.menu.popup_image_selection, mMenu.getMenu());
        mMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent i = null;
                switch (item.getItemId()) {
                    case R.id.OptionBeatPoint:
                        i = new Intent(BeatPointListActivity.this, AddBeatPoint.class);
                        i.putExtra("option","BP");
                        startActivity(i);

                        break;

                    case R.id.OptionBadCharacter:
                        i = new Intent(BeatPointListActivity.this, AddBeatPoint.class);
                        i.putExtra("option","BC");
                        startActivity(i);
                        break;

                    case R.id.OptionQrCode:
                        i = new Intent(BeatPointListActivity.this, NewQRScanActivity.class);
                        i.putExtra("option","QR");
                        startActivity(i);
                        break;
                }
                return false;
            }
        });
        mMenu.show();
    }

}
