package com.quad.track;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ShutDownReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "ShutDownReceiver";

    //private boolean isConnected = false;
    @Override
    public void onReceive(final Context context, final Intent intent) {

        isPhoneShuttingDown(context);

    }


    private void isPhoneShuttingDown(Context context) {

        DatabaseHandler databaseHandler = new DatabaseHandler(context);
        System.out.println(" Shutting down .....");
        try {
            Date date = new Date();
            DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
            String currentDateTimeString = utilDateFormatter.format(date);
            databaseHandler.addData("OFF", currentDateTimeString, "POWER");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


}