package com.quad.track;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NetworkChangeReceiver extends BroadcastReceiver {
	 
	 private static final String LOG_TAG = "CheckNetworkStatus";
	 private boolean isConnected = false;
	  @Override
	  public void onReceive(final Context context, final Intent intent) {
	 
	   isNetworkAvailable(context);
	   
	  }
	 
	 
	  private boolean isNetworkAvailable(Context context) {
	   ConnectivityManager connectivity = (ConnectivityManager)
	     context.getSystemService(Context.CONNECTIVITY_SERVICE);
	   
	   DatabaseHandler databaseHandler=new DatabaseHandler(context);
	  
	   
	   if (connectivity != null) {
	    NetworkInfo[] info = connectivity.getAllNetworkInfo();
	    if (info != null) {
	     for (int i = 0; i < info.length; i++) {
	      if (info[i].getState() == NetworkInfo.State.CONNECTED) {
	       if(!isConnected){
	        
	        try {
				Date date = new Date();
				DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
				String currentDateTimeString = utilDateFormatter.format(date);
				databaseHandler.addData("ON", currentDateTimeString, "INTERNET");
			} catch (Exception e) {
				
			}
	        
	       
	        
	        isConnected = true;
	       
	       }
	       return true;
	      }
	     }
	    }
	   }
	   
	    try {
			Date date = new Date();
			DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
			String currentDateTimeString = utilDateFormatter.format(date);
			databaseHandler.addData("OFF", currentDateTimeString, "INTERNET");
		} catch (Exception e) {
			
		}
	    
	    isConnected = false;
	    return false;
	  }
	 }