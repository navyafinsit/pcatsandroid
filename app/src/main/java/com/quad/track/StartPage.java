package com.quad.track;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//import com.google.android.maps.GeoPoint;

public class StartPage extends Activity {
	Handler handler;
	private static final String TAG = "Start Page";
	protected int TAKE_PICTURE = 101;
	//GeoPoint snapLoc;
	boolean success;
	String fileName = System.currentTimeMillis() + "_bmic.jpg";
	Date date = new Date();
	ImageView showImage;
	EditText detail_edit;
	String status="S";
	Integer id;
	String datFileStored;
	Bitmap bmap;
	byte[] imageInByte;
	TextView t_lat;
	TextView t_long;
	Button uploadBtn;
	Bitmap myBitmap;
	String selectedDealerId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Context myContext=null;
		try {
			myContext = createPackageContext("com.quad.track", Context.CONTEXT_IGNORE_SECURITY);
			SharedPreferences data;

			data = myContext.getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
			String deviceId=data.getString("deviceid",null);
			if(deviceId!=null) {
				DataUploader.deviceId=deviceId;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}


		Intent startIntent = new Intent();
		startIntent.setComponent(new ComponentName("com.quad.track","com.quad.track.GPSService"));
		//myContext.stopService(startIntent);
		myContext.startService(startIntent);


		handler = new Handler();
		setContentView(R.layout.startpage);
		showImage = (ImageView) findViewById(R.id.selectedImageView);
		System.out.println("Entered into on create method ");
		Intent intent = getIntent();
		selectedDealerId=intent.getStringExtra("dealerId");

/*		if(intent!=null){
			System.out.println("Intent recieved");
	    	//bmap=(Bitmap)intent.getParcelableExtra("FingerBitmapImage");
			bmap=FtrScanDemoUsbHostActivity.fingerPrintImageBMP;
			showImage.setImageBitmap(bmap);
	    	DataUploader.deviceId=intent.getStringExtra("deviceId");
		}else{
			System.out.println("no intent data recieved");
		}*/
		//DataUploader.deviceId=intent.getStringExtra("deviceId");
		/*Button startCapture = (Button) findViewById(R.id.startCaptureBtn);
		startCapture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				System.out.println(DataUploader.yyyy + "*********************");
				startActivity(new Intent("com.futronictech.CameraDemo"));
			}
		});*/
		ImageButton startCapture2 = (ImageButton) findViewById(R.id.startCaptureBtn2);
		startCapture2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ActivityList.setCurActivity(ActivityList.CAMERA);
				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
				startActivityForResult(intent, 0);
			}
		});
		/*Button loadImage = (Button) findViewById(R.id.loadFile);
		loadImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ActivityList.setCurActivity(ActivityList.BROWSER);
				Intent intent = new Intent("com.futronictech.AndroidExplorer");
				startActivityForResult(intent, 0);
			}
		});*/
		/*Button setLoc = (Button) findViewById(R.id.setLocBtn);*/
		/*setLoc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// startActivity(new Intent("com.quad.giet.ShowMap"));
				// startActivity(new Intent("com.quad.giet.PrintQRMainActivity"));
				if(showImage.getDrawable()==null || myBitmap==null){
					System.out.println("IMAGE IS NULL");
					Toast.makeText(StartPage.this,"Please capture the image first", Toast.LENGTH_LONG).show();
					return;
				}else{
					startActivityForResult(
							new Intent("com.futronictech.PrintQRMainActivity"),
							ActivityList.SELECTMARKER);
				}

			}
		});*/
		// spinner for am and pm
		/*Spinner ampmSpinner = (Spinner) findViewById(R.id.spinner4);
		ArrayAdapter<CharSequence> ampmChoice = ArrayAdapter.createFromResource(this, R.array.am_pm,android.R.layout.simple_spinner_item);
		ampmChoice.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ampmSpinner.setAdapter(ampmChoice);
		if (status!=null && status.equalsIgnoreCase("F")){
			if (!DataUploader.ampm.equals(null)) {
		        int spinnerPostion = ampmChoice.getPosition(DataUploader.ampm);
		        ampmSpinner.setSelection(spinnerPostion);
		    }
		}*/

		// spinner for list of crimes
		Spinner crimesListSpinner = (Spinner) findViewById(R.id.crimeSpinner);
		ArrayAdapter<CharSequence> crimesListChoice = ArrayAdapter.createFromResource(this, R.array.crimes,android.R.layout.simple_spinner_item);
		crimesListChoice.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		crimesListSpinner.setAdapter(crimesListChoice);
		if (status!=null && status.equalsIgnoreCase("F")){
			if (!DataUploader.title.equals(null)) {
				int crimesSpinnerPostion = crimesListChoice.getPosition(DataUploader.title);
				crimesListSpinner.setSelection(crimesSpinnerPostion);
			}
		}

		/*
		 * crimesListSpinner.setOnItemSelectedListener(new
		 * AdapterView.OnItemSelectedListener() { public void
		 * onItemSelected(AdapterView<?> arg0, View v, int position, long id) {
		 * Log.v("routes", "route selected");
		 *
		 * }
		 *
		 * public void onNothingSelected(AdapterView<?> arg0) { Log.v("routes",
		 * "nothing selected"); } });
		 */

		/*
		 * EditText title_edit = (EditText)findViewById(R.id.titleBox);
		 * title_edit.setOnFocusChangeListener(new View.OnFocusChangeListener()
		 * {
		 *
		 * @Override public void onFocusChange(View v, boolean hasFocus) {
		 * if(hasFocus == false) { EditText title_edit = (EditText)v;
		 * DataUploader.title = title_edit.getText().toString(); }
		 *
		 * } });
		 */

		detail_edit = (EditText) findViewById(R.id.detailBox);
		detail_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus == false) {
					EditText detail_edit = (EditText) v;
					DataUploader.detail = detail_edit.getText().toString();
				}

			}
		});

		uploadBtn = (Button) findViewById(R.id.uploadBtn);
		//Button retryUploadBtn = (Button) findViewById(R.id.retryUploadBtn);
		if (status!=null && status.equalsIgnoreCase("F")) {
			uploadBtn.setVisibility(View.INVISIBLE);
		}else{
			//	retryUploadBtn.setVisibility(View.INVISIBLE);
		}
		uploadBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getApplicationContext(),"uploading",5).show();
				showBeatQRPoints();

				/*
				 * Intent i=new Intent(StartPage.this,SuccessActivity.class);
				 * i.putExtra("success", success); startActivity(i);
				 */
			}
		});

/*		retryUploadBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getApplicationContext(),"uploading",5).show();

				final ProgressDialog progress = ProgressDialog.show(
						StartPage.this, "Crime Uploading", "Please wait...",
						true);

				new Thread() {
					public void run() {
						System.out.println("UPLODED THREAD CALLED");
						try {

							EditText detail_edit = (EditText) findViewById(R.id.detailBox);
							DataUploader.detail = detail_edit.getText()
									.toString();
							Spinner crimesListSpinner = (Spinner) findViewById(R.id.crimeSpinner);
							DataUploader.title = crimesListSpinner
									.getSelectedItem().toString();
							Spinner ampmSpinner = (Spinner) findViewById(R.id.spinner4);
							DataUploader.ampm = ampmSpinner.getSelectedItem()
									.toString();
							// success=DataUploader.upload();
							success = DataUploader.uploadToServer();
							DBHelper dbh = new DBHelper(getApplicationContext());
							byte[] img = null;

							String time = "";
							String status = "";
							String tempHours = DataUploader.hr+"".toString().trim();
							String tempMinutes = DataUploader.min+"".toString().trim();
							int hours = Integer.parseInt(tempHours);
							int minutes = Integer.parseInt(tempMinutes);
							if(minutes < 10){
								tempMinutes="0"+minutes;
							}else if(minutes>60){
								Toast.makeText(getApplicationContext(), "Invalid Date", Toast.LENGTH_SHORT).show();
							}
							if (hours > 12) {
								hours = hours - 12;
								tempHours=hours+"".toString();
								if(hours < 10){
									tempHours="0"+hours+"".toString();
								}
								else{
									tempHours = hours+"".toString().trim();
								}
							}
							else if(hours == 12){
								tempHours=12+"".toString().trim();
								//time = 12 + ":" + DataUploader.min+ DataUploader.ampm;
							}
							time=tempHours+":"+tempMinutes+DataUploader.ampm;
							System.out.println("time is" + time+ "*-*-*-*-*-*-*");
							String year = DataUploader.yyyy+ "".toString().trim();
							String month = DataUploader.mm+ "".toString().trim();
							String day = DataUploader.dd + "".toString().trim();

							Bitmap b = BitmapFactory
									.decodeFile(DataUploader.uploadImageUri);
							ByteArrayOutputStream bos = new ByteArrayOutputStream();
							b.compress(CompressFormat.PNG, 100, bos);
							img = bos.toByteArray();

							if (success == true) {
								try{
									System.out.println("Entered into success state updating failure state");
									System.out.println("*-*-*-*" + month + day
											+ time + DataUploader.latitude
											+ "*-*-*-*-*-*-*-");
									//updating crime status in sqlite database
									dbh.updateCrime(id,year, month, day, time,
											DataUploader.latitude,
											DataUploader.longitude,
											DataUploader.title,
											DataUploader.detail,
											DataUploader.imagePath, img, "S");

								}
								catch(Exception e){
									e.printStackTrace();
									progress.dismiss();
								}

							} else {
								System.out.println("Entered into retry upload failure state");
								Toast.makeText(getApplicationContext(), "Uploading failed, Try agin", Toast.LENGTH_SHORT).show();
								progress.dismiss();

							}
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									progress.dismiss();

									Intent i = new Intent(StartPage.this,
											SuccessActivity.class);
									i.putExtra("success", success);
									startActivity(i);
								}
							});

							*//*
							 * if(success){
							 * Toast.makeText(getApplicationContext(),
							 * "Thanks for uploading FIR complaint",
							 * Toast.LENGTH_SHORT).show(); } else{
							 * Toast.makeText(getApplicationContext(),
							 * "Uploading faied try again",
							 * Toast.LENGTH_SHORT).show(); }
							 *//*

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}.start();

				*//*
				 * Intent i=new Intent(StartPage.this,SuccessActivity.class);
				 * i.putExtra("success", success); startActivity(i);
				 *//*
			}
		});*/
	}

	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS Settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Go to settings menu?");
		alertDialog.setCancelable(false);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);

			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Showing Alert Message
		AlertDialog alertDialog1=alertDialog.create();
		alertDialog1.show();
	}

	public boolean calculateTime(String inTime,String outTime)
	{


		Calendar cal = Calendar.getInstance();

		Time currentTime = Time.valueOf(
				cal.get(Calendar.HOUR_OF_DAY) + ":" +
						cal.get(Calendar.MINUTE) + ":" +
						cal.get(Calendar.SECOND) );


		try{
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

			long ms1 = sdf.parse(inTime).getTime();
			Time t1 = new Time(ms1);
			long ms2 = sdf.parse(outTime).getTime();
			Time t2 = new Time(ms2);

			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(t1);
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(t2);

			int x1=Calendar.PM;
			int y1=cal1.get(Calendar.AM_PM);
			int x2=Calendar.AM;
			int y2=cal2.get(Calendar.AM_PM);


			if(x1==y1 && x2==y2) // THIS IS FOR PM AM
			{
				//CALENDAR FOR 12 AM
				Calendar cal12AM = Calendar.getInstance();
				//cal12AM.add(Calendar.DATE,1);// ADDING 1 TO GET ZERO HOUR OF NEXT DAY....
				cal12AM.set(Calendar.HOUR_OF_DAY,23);
				cal12AM.set(Calendar.MINUTE,59);
				cal12AM.set(Calendar.SECOND,59);
				Time zeroTime1=Time.valueOf(	cal12AM.get(Calendar.HOUR_OF_DAY) + ":" +
						cal12AM.get(Calendar.MINUTE) + ":" +
						cal12AM.get(Calendar.SECOND) );

				Time zeroTime2=Time.valueOf("00:00:00");

				if(currentTime.after(t1) && currentTime.before(zeroTime1) ||  currentTime.after(zeroTime2) && currentTime.before(t2))
				{
					return true;
				}else
				{
					return false;
				}
			}



			if(currentTime.after(t1) && currentTime.before(t2)) // this if for AM AM , AM PM , PM PM
			{
				return true;
			}else
			{
				return false;
			}

		}catch(java.text.ParseException pe)
		{
			pe.printStackTrace();
		}



		return false;

	}
	public void showBeatQRPoints() {
		DataUploader.routeCoordinateId="0";
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// getting GPS status
		boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if(!isGPSEnabled)
		{
			showSettingsAlert();
		}else {

			final Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

			if (location == null) {
				Toast.makeText(getApplicationContext(), "Sorry Your location is not available", Toast.LENGTH_LONG).show();
				return;
			}
		/*	DBHelper db = new DBHelper(getApplicationContext());

			ArrayList<DealerBean> nearestQRcodes = db.getNearestBeatsAndQRCodes(location.getLatitude(), location.getLongitude());
			db.close();

			final BeatPointListAdapter beatPointAdapter=new BeatPointListAdapter(this,nearestQRcodes);

			AlertDialog.Builder builderSingle = new AlertDialog.Builder(StartPage.this);
			//builderSingle.setIcon(R.drawable.icon);
			builderSingle.setTitle("Select One Name:-");*/

			//final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(StartPage.this, android.R.layout.select_dialog_singlechoice);

		/*for(DealerBean dbean:nearestQRcodes )
		{
			//arrayAdapter.add(dbean.getDealerName());
			beatPointAdapter.
		}


*/

			DataUploader.routeCoordinateId=selectedDealerId;

			// added by dinesh...

			try{

				if(showImage.getDrawable()==null || myBitmap==null){
					System.out.println("IMAGE IS NULL");
					Toast.makeText(StartPage.this,"Please Capture the image", Toast.LENGTH_LONG).show();
					return;
				}
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				Location lastKnownLocation = null;
				if(isGPSEnabled) {
					lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				}

				if(lastKnownLocation==null) {
					Toast.makeText(StartPage.this, "PLEASE ENABLE YOUR GPS AND TRY AGAIN", Toast.LENGTH_LONG).show();
					return;
				}else{

					t_lat.setText(lastKnownLocation.getLatitude()+"");
					t_long.setText(lastKnownLocation.getLongitude()+"");
				}

				String detailField = detail_edit.getText().toString();
				if("".equalsIgnoreCase(detailField)){
					Toast.makeText(StartPage.this, "Please enter details", Toast.LENGTH_LONG).show();
					//detail_edit.setError("Please enter details");
					return;
				}


			}
			catch(Exception e){
				e.printStackTrace();
				return;
			}

			final ProgressDialog progress = ProgressDialog.show(
					StartPage.this, "Beat Photo Uploading", "Please wait...",
					true);


			new Thread() {
				public void run() {
					System.out.println("UPLOAD THREAD CALLED");

					try {
						//Save the fingerprint bitmap to file.
						saveBitmapToFile(bmap);
						EditText detail_edit = (EditText) findViewById(R.id.detailBox);
						DataUploader.detail = detail_edit.getText().toString();
						Spinner crimesListSpinner = (Spinner) findViewById(R.id.crimeSpinner);
						DataUploader.title = crimesListSpinner.getSelectedItem().toString();
						//Spinner ampmSpinner = (Spinner) findViewById(R.id.spinner4);
						//DataUploader.ampm = ampmSpinner.getSelectedItem().toString();
						// success=DataUploader.upload();

						DataUploader.latitude=t_lat.getText().toString();
						DataUploader.longitude=t_long.getText().toString();


						success = DataUploader.uploadPictureToServer(DataUploader.uploadImageUri);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								progress.dismiss();
								if(success==true){
									Intent i = new Intent(StartPage.this,SuccessActivity.class);
									i.putExtra("success", success);
									startActivity(i);
									finish();
								}else
								{
									Toast.makeText(StartPage.this, "Upload Failed. Please try back again", Toast.LENGTH_LONG).show();
								}

							}
						});



					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}.start();

			// end of added code by dinesh
/*

			builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			builderSingle.setAdapter(beatPointAdapter, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					final DealerBean bean = (DealerBean)beatPointAdapter.getItem(which);
					AlertDialog.Builder builderInner = new AlertDialog.Builder(StartPage.this);
					builderInner.setMessage(bean.getDealerName());
					builderInner.setTitle("Your Selected Point is");
					builderInner.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							return;
						}
					});
					builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							*//*DataUploader.routeCoordinateId=bean.getDealerId();

							// added by dinesh...

							try{

								if(showImage.getDrawable()==null || myBitmap==null){
									System.out.println("IMAGE IS NULL");
									Toast.makeText(StartPage.this,"Please Capture the image", Toast.LENGTH_LONG).show();
									return;
								}
								LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
								boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
								Location lastKnownLocation = null;
								if(isGPSEnabled) {
									lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
								}

								if(lastKnownLocation==null) {
									Toast.makeText(StartPage.this, "PLEASE ENABLE YOUR GPS AND TRY AGAIN", Toast.LENGTH_LONG).show();
									return;
								}else{

									t_lat.setText(lastKnownLocation.getLatitude()+"");
									t_long.setText(lastKnownLocation.getLongitude()+"");
								}

												String detailField = detail_edit.getText().toString();
								if("".equalsIgnoreCase(detailField)){
									Toast.makeText(StartPage.this, "Please enter details", Toast.LENGTH_LONG).show();
									//detail_edit.setError("Please enter details");
									return;
								}


							}
							catch(Exception e){
								e.printStackTrace();
								return;
							}

							final ProgressDialog progress = ProgressDialog.show(
									StartPage.this, "Beat Photo Uploading", "Please wait...",
									true);*//*

						*//*	new Thread() {
								public void run() {
									System.out.println("UPLOAD THREAD CALLED");

									try {
										//Save the fingerprint bitmap to file.
										saveBitmapToFile(bmap);
										EditText detail_edit = (EditText) findViewById(R.id.detailBox);
										DataUploader.detail = detail_edit.getText().toString();
										Spinner crimesListSpinner = (Spinner) findViewById(R.id.crimeSpinner);
										DataUploader.title = crimesListSpinner.getSelectedItem().toString();
										//Spinner ampmSpinner = (Spinner) findViewById(R.id.spinner4);
										//DataUploader.ampm = ampmSpinner.getSelectedItem().toString();
										// success=DataUploader.upload();

										DataUploader.latitude=t_lat.getText().toString();
										DataUploader.longitude=t_long.getText().toString();


										success = DataUploader.uploadPictureToServer(DataUploader.uploadImageUri);
										runOnUiThread(new Runnable() {
											@Override
											public void run() {
												progress.dismiss();
												if(success==true){
													Intent i = new Intent(StartPage.this,SuccessActivity.class);
													i.putExtra("success", success);
													startActivity(i);
													finish();
												}else
												{
													Toast.makeText(StartPage.this, "Upload Failed. Please try back again", Toast.LENGTH_LONG).show();
												}

											}
										});



									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}.start();

							// end of added code by dinesh
*//*



						}
					});
					builderInner.show();
				}
			});
			builderSingle.show();*/
		}
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		/*
		 * TextView t_long = (TextView)findViewById(R.id.longitude_show);
		 * TextView t_lat = (TextView)findViewById(R.id.latitude_show);
		 * t_lat.setText(String.format("%.2f", DataUploader.geocode[0]));
		 * t_long.setText(String.format("%.2f", DataUploader.geocode[1]));
		 */
		// TextView addr = (TextView)findViewById(R.id.addr_show);
		// addr.setText(DataUploader.locAddr);
	}

	@Override
	public void onActivityResult(final int requestCode, final int resultCode,
								 final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Thread t = new Thread() {
			public void run() {
				fileName=fileName.replace("/storage/sdcard0/","");
				switch (ActivityList.curActivity) {
					case ActivityList.CAMERA:
						if (resultCode == RESULT_CANCELED) {
							showToast("camera cancelled", 10000);
							return;
						}
						// lets check if we are really dealing with a picture
						if (requestCode == 0 && resultCode == RESULT_OK) {
							Bundle extras = data.getExtras();
							Bitmap b = (Bitmap) extras.get("data");
							if(b==null){
								System.out.println("PLEASE TAKE THE PICTURE");
								Toast.makeText(StartPage.this, "Please capture the image", Toast.LENGTH_LONG).show();
								return;
							}
							// setContentView(R.layout.startpage);
							File sdCard = Environment.getExternalStorageDirectory();
							File file = new File(sdCard, fileName);
							System.out.println("*-*-*-*file name is-*-*-*--"+ fileName);
							FileOutputStream fos = null;
							try {
								file.createNewFile();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							try {
								fos = new FileOutputStream(file);

							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							b.compress(CompressFormat.JPEG, 95, fos);
							String timestamp = Long.toString(System.currentTimeMillis());
							MediaStore.Images.Media.insertImage(getContentResolver(), b, timestamp, timestamp);
							//fileName = "/storage/sdcard0/" + fileName;

						/*
						 * StartPage.this.runOnUiThread(new Runnable() { public
						 * void run() { TextView t_long =
						 * (TextView)findViewById(R.id.longitude_show); TextView
						 * t_lat = (TextView)findViewById(R.id.latitude_show);
						 * t_lat.setText(data.getStringExtra("LATITUDE"));
						 * t_long.setText(data.getStringExtra("LONGITUDE"));
						 * DataUploader
						 * .latitude=data.getStringExtra("LATITUDE");
						 * DataUploader
						 * .longitude=data.getStringExtra("LONGITUDE"); } });
						 */

						}

						break;

					case ActivityList.BROWSER:

						if (requestCode == ActivityList.SELECTMARKER) {

							StartPage.this.runOnUiThread(new Runnable() {
								public void run() {

									DataUploader.latitude = data
											.getStringExtra("LATITUDE");
									DataUploader.longitude = data
											.getStringExtra("LONGITUDE");

									//t_long = (TextView) findViewById(R.id.longitude_show);
									//t_lat = (TextView) findViewById(R.id.latitude_show);
									t_lat.setText(data.getStringExtra("LATITUDE"));
									t_long.setText(data.getStringExtra("LONGITUDE"));
								}
							});

							return;
						}

						if (resultCode == RESULT_CANCELED) {
							showToast("No File selected cancelled", 10000);
							return;
						} else {
							if (data != null) {
								Bundle extras = data.getExtras();
								if (extras != null) {
									String s = (String) extras.getString("filePath");
									fileName = s;
									try {
										Bitmap myBitmap = BitmapFactory.decodeFile(fileName);
										MediaStore.Images.Media.insertImage(getContentResolver(), myBitmap,"BMIC image", "Image for bmic");
									} catch (Exception e) {
										showToast("Image does not exist!!!!", Toast.LENGTH_SHORT);
									}
								} else {
									Log.d(TAG, "Extras is null");
								}
							} else {
								Log.d(TAG, "Data is null");
							}
						}
				}
				//File imageFile = new File(fileName);

				File sdCard = Environment.getExternalStorageDirectory();
				File imageFile = new File(sdCard, fileName);

				System.out.println(fileName + "*************************");
				try {
					if (!imageFile.exists()) {

						fileName = "/storage/sdcard0/" + fileName;
						imageFile = new File(fileName);

						if (!imageFile.exists()) {
							showToast("Image does not exist!!",
									Toast.LENGTH_SHORT);
							return;
						}

					} else if (!imageFile.canRead()) {
						showToast("Image cannot be read!", Toast.LENGTH_SHORT);
						return;
					}
				} catch (Exception e) {
					showToast("Not a valid Image File!", Toast.LENGTH_SHORT);
					return;
				}

				DataUploader.uploadImageUri = imageFile.toString();
				myBitmap = getPreview(DataUploader.uploadImageUri,showImage.getWidth());
				//SHOWING IMAGE
				//showImage.setImageBitmap(myBitmap);

				ExifInterface exif;
				try {
					exif = new ExifInterface(imageFile.getAbsolutePath());
					String dateTime = exif
							.getAttribute(ExifInterface.TAG_DATETIME);
					try {
						if (dateTime == null) {
							// showToast("No Date time Information found!",Toast.LENGTH_LONG);
							throw new Exception("No Date Found");
						} else {
							String[] dt = dateTime.split(" ");
							String[] date = dt[0].split(":");
							String[] time = dt[1].split(":");
							DataUploader.yyyy = Integer.parseInt(date[0]);
							DataUploader.mm = Integer.parseInt(date[1]);
							DataUploader.dd = Integer.parseInt(date[2]);
							DataUploader.hr = Integer.parseInt(time[0]);
							DataUploader.min = Integer.parseInt(time[1]);
						}
					} catch (Exception e) {
						Calendar cal = Calendar.getInstance();
						DataUploader.yyyy = cal.get(Calendar.YEAR);
						DataUploader.mm = cal.get(Calendar.MONTH) + 1;
						DataUploader.dd = cal.get(Calendar.DATE);
						DataUploader.hr = cal.get(Calendar.HOUR_OF_DAY);
						DataUploader.min = cal.get(Calendar.MINUTE);
						System.out.println("year:" + DataUploader.yyyy
								+ "minutes" + DataUploader.min);
					}
					String orientation = exif
							.getAttribute(ExifInterface.TAG_ORIENTATION);
					if (orientation == null) {
						orientation = "vertical";
					}
					exif.getLatLong(DataUploader.geocode);

				} catch (IOException e) { // dinesh...IOEXception earlier..
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				StartPage.this.runOnUiThread(new Runnable() {
					public void run() {
						showImage.setScaleType(ImageView.ScaleType.FIT_XY);
						showImage.setImageBitmap(myBitmap);
						uploadBtn.setVisibility(View.VISIBLE);
						CharSequence[] yearList = new CharSequence[10];
						for (int i = 0; i < 10; i++) {
							yearList[i] = Integer.toString(DataUploader.yyyy
									- i);
						}
						//Spinner yearChoiceSpinner = (Spinner) findViewById(R.id.spinnerYear);
				/*		ArrayAdapter<CharSequence> yearChoice = new ArrayAdapter<CharSequence>(
								getApplicationContext(),
								android.R.layout.simple_spinner_item, yearList);
						yearChoice
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						yearChoiceSpinner.setAdapter(yearChoice);
						yearChoiceSpinner.setSelection(0);*/
						CharSequence[] monList = new CharSequence[12];
						for (int i = 0; i < 12; i++) {
							monList[i] = Integer.toString(i + 1);
						}
						/*Spinner monChoiceSpinner = (Spinner) findViewById(R.id.spinnerMonth);
						ArrayAdapter<CharSequence> monChoice = new ArrayAdapter<CharSequence>(
								getApplicationContext(),
								android.R.layout.simple_spinner_item, monList);
						monChoice
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						monChoiceSpinner.setAdapter(monChoice);
						monChoiceSpinner.setSelection(DataUploader.mm - 1);
*/
						CharSequence[] dayList = new CharSequence[31];
						for (int i = 0; i < 31; i++) {
							dayList[i] = Integer.toString(i + 1);
						}
						/*Spinner dayChoiceSpinner = (Spinner) findViewById(R.id.spinnerDay);
						ArrayAdapter<CharSequence> dayChoice = new ArrayAdapter<CharSequence>(
								getApplicationContext(),
								android.R.layout.simple_spinner_item, dayList);
						dayChoice
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						dayChoiceSpinner.setAdapter(dayChoice);
						dayChoiceSpinner.setSelection(DataUploader.dd - 1);

						EditText t = (EditText) findViewById(R.id.timeHour);
						t.setText(Integer.toString(DataUploader.hr % 12));
						if (DataUploader.hr >= 12) {
							Spinner am_pm = (Spinner) findViewById(R.id.spinner4);
							am_pm.setSelection(1);
						}
						EditText t2 = (EditText) findViewById(R.id.timeMin);
						t2.setText(Integer.toString(DataUploader.min));
*/
						t_long = (TextView) findViewById(R.id.longitude_show);
						t_lat = (TextView) findViewById(R.id.latitude_show);
						t_lat.setText(data.getStringExtra("LATITUDE"));
						t_long.setText(data.getStringExtra("LONGITUDE"));
						DataUploader.latitude = t_lat.getText().toString();
						DataUploader.longitude = t_long.getText().toString();
						System.out.println(" $$$$$$$$$$$$$$$$$$ ");
					}
				});
			}
		};

		t.start();

	}

	Bitmap getPreview(String uri, int width) {
		File image = new File(uri);

		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(image.getPath(), bounds);
		if ((bounds.outWidth == -1) || (bounds.outHeight == -1))
			return null;

		int originalSize = bounds.outWidth;

		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = originalSize / (width);
		return BitmapFactory.decodeFile(image.getPath(), opts);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater mi = getMenuInflater();
		mi.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
			case R.id.settings_menu_item:
				startActivity(new Intent("com.quad.giet.com.Home"));
				break;
			case R.id.exit_menu_item:
				finish();
				break;
		}
		return true;
	}

	public void showToast(final String toastMessage, final int duration) {
		handler.post(new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), toastMessage, duration)
						.show();
			}
		});
	}


	public void saveBitmapToFile(Bitmap fingerPrint){
		//Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
		try{
			Bitmap bitmap = fingerPrint;
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.JPEG, 100, stream);
			bitmap.setDensity(500);
			imageInByte = stream.toByteArray();
			System.out.println(imageInByte);
			//ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);
			datFileStored="/storage/sdcard0/template.dat";
			FileOutputStream fos = new FileOutputStream(datFileStored);
			fos.write(imageInByte);
			fos.close();
			stream.close();
			//tvInfo.setText(output);
			//Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
/*	public String upload(String i_file) 
	{
		String responseString = null;
		try
		{
			System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER");
			if(true){
		    HttpClient httpclient = new DefaultHttpClient();
		    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		    String imagePath="Template"+System.currentTimeMillis();
		    HttpPost httppost = new HttpPost("http://43.254.42.12:2018/fingerserver/EnrollPictureFromClient?imageUrl="+imagePath+"&criminalDescription="+criminalDescription+"&deviceId="+deviceId);
		    File file = new File(i_file);
		    MultipartEntity mpEntity = new MultipartEntity();
		    ContentBody cbFile = new FileBody(file, "image/jpeg");
		    mpEntity.addPart("userfile", cbFile);
		    httppost.setEntity(mpEntity);
		    System.out.println("executing request " + httppost.getRequestLine());
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity resEntity = response.getEntity();
		    InputStream stream=resEntity.getContent();
		    responseString = convertStreamToString(stream);
		    System.out.println("RESPONSE STRING IS:"+responseString);
		    System.out.println(response.getStatusLine());
		    if (resEntity != null) {
		      resEntity.consumeContent();
		    }
		    httpclient.getConnectionManager().shutdown();
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return responseString;
	    
	}
	
	public String convertStreamToString(InputStream is) throws Exception {

	    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        sb.append(line + "\n");
	    }
	    is.close();
	    return sb.toString();
	}*/

}
