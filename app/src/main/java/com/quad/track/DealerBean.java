package com.quad.track;

import java.io.Serializable;
import java.util.Date;

public class DealerBean implements Serializable {

	private int alarmId;
	private String dealerId;
	private String dealerName;
	private String latitude;
	private String longitude;
	private int state;
	private String empid;
	private long insertRowId;
	private Date inTime,outTime;
	private double timeSpent;
	private boolean inside;	
	private Date date;

	private boolean disabled;
	private String definedInTime;
	private String definedOutTime;
	private String beatPointType;
	private String visited;
	private String qrcode;


	public String getVisited() {
		return visited;
	}

	public void setVisited(String visited) {
		this.visited = visited;
	}

	public DealerBean() {
		super();
	}
	public DealerBean(int id){
		super();
		this.alarmId=id;
	}

	//overloaded method added by dinesh.
	/*public DealerBean(int alarmId, String dealerId, String dealerName, String latitude, String longitude, int state, String empid,boolean disabled,String definedInTime,String definedOutTime) {
		super();
		this.alarmId = alarmId;
		this.dealerId = dealerId;
		this.dealerName = dealerName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.state = state;
		this.empid=empid;
		this.definedInTime=definedInTime;
		this.definedOutTime=definedOutTime;
		this.disabled=disabled;
	}*/

	public DealerBean(int alarmId, String dealerId, String dealerName, String latitude, String longitude, int state, String empid,boolean disabled,String definedInTime,String definedOutTime,String beatPointType) {
		super();
		this.alarmId = alarmId;
		this.dealerId = dealerId;
		this.dealerName = dealerName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.state = state;
		this.empid=empid;
		this.definedInTime=definedInTime;
		this.definedOutTime=definedOutTime;
		this.disabled=disabled;
		this.beatPointType=beatPointType;

	}

	/*public DealerBean(int alarmId, String dealerId, String dealerName, String latitude, String longitude, int state, String empid) {
		super();
		this.alarmId = alarmId;
		this.dealerId = dealerId;
		this.dealerName = dealerName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.state = state;
		this.empid=empid;
	}*/
/*	public DealerBean(String dealerId, String dealerName, String latitude, String longitude, int state, String empid) {
		super();
		this.dealerId = dealerId;
		this.dealerName = dealerName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.state = state;
		this.empid=empid;
	}*/
	/*public DealerBean(int alarmId, Date date, String dealerId, String empid, Date inTime, Date outTime, double timeSpent) {
		super();
		this.alarmId = alarmId;
		this.date = date;
		this.dealerId = dealerId;
		this.empid = empid;
		this.inTime = inTime;
		this.outTime = outTime;
		this.timeSpent = timeSpent;
	}*/

	public DealerBean(int alarmId, Date date, String dealerId, String empid, Date inTime, Date outTime, double timeSpent) {
		super();
		this.alarmId = alarmId;
		this.date = date;
		this.dealerId = dealerId;
		this.empid = empid;
		this.inTime = inTime;
		this.outTime = outTime;
		this.timeSpent = timeSpent;
		this.disabled=disabled;
		this.definedInTime=definedInTime;
		this.definedOutTime=definedOutTime;
	}


	public int getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(int alarmId) {
		this.alarmId = alarmId;
	}

	public String getDealerId() {
		return dealerId;
	}
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public long getInsertRowId() {
		return insertRowId;
	}
	public void setInsertRowId(long insertRowId) {
		this.insertRowId = insertRowId;
	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public boolean isInside() {
		return inside;
	}
	public void setInside(boolean inside) {
		this.inside = inside;
	}
	public Date getInTime() {
		return inTime;
	}
	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}
	public Date getOutTime() {
		return outTime;
	}
	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}
	public double getTimeSpent() {
		return timeSpent;
	}
	public void setTimeSpent(double timeSpent) {
		this.timeSpent = timeSpent;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public String getDefinedInTime() {
		return definedInTime;
	}

	public void setDefinedInTime(String definedInTime) {
		this.definedInTime = definedInTime;
	}

	public String getDefinedOutTime() {
		return definedOutTime;
	}

	public void setDefinedOutTime(String definedOutTime) {
		this.definedOutTime = definedOutTime;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/*@Override
	public String toString() {
		return "AlarmList [alarmId=" + alarmId + ", dealerId="
				+ dealerId + ", dealerName=" + dealerName
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", state=" + state + "]";
	}*/

	@Override
	public String toString() {
		return "DealerBean{" +
				"alarmId=" + alarmId +
				", dealerId='" + dealerId + '\'' +
				", dealerName='" + dealerName + '\'' +
				", latitude='" + latitude + '\'' +
				", longitude='" + longitude + '\'' +
				", state=" + state +
				", empid='" + empid + '\'' +
				", insertRowId=" + insertRowId +
				", inTime=" + inTime +
				", outTime=" + outTime +
				", timeSpent=" + timeSpent +
				", inside=" + inside +
				", date=" + date +
				", disabled=" + disabled +
				", definedInTime='" + definedInTime + '\'' +
				", definedOutTime='" + definedOutTime + '\'' +
				'}';
	}


	public String getBeatPointType() {
		return beatPointType;
	}

	public void setBeatPointType(String beatPointType) {
		this.beatPointType = beatPointType;
	}

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
}
